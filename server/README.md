# 🖥️ Server

## 🏃‍♂️ run

### 💻 local

Run the following commands to start the server in a local environment:

```bash
poetry install
s3rver --artifact-dir sample/artifacts --metadata-dir sample/metadata

# Example:
# s3rver --artifact-dir sample/artifacts --metadata-dir sample/metadata \
#   --database-url 'postgresql://postgres:secret@0.0.0.0:5433/postgres'
```

### 🐳 docker

Use Docker to build and run the server:

```bash
# Build
docker build -t s3rver:latest .

# Check
docker run --rm s3rver:latest --help
# Prints:
# Usage: s3rver [OPTIONS]

# Options:
#   --artifact-dir TEXT   [default: artifacts]
#   --metadata-dir TEXT
#   --port INTEGER        [default: 8010]
#   --host TEXT           [default: 127.0.0.1]
#   --database-url TEXT   [default: sqlite:///./test.db]
#   --install-completion  Install completion for the current shell.
#   --show-completion     Show completion for the current shell, to copy it or
#                         customize the installation.
#   --help                Show this message and exit.

# Run
docker run -d --rm --name temp_container \
    -v ${pwd}/sample/metadata:/app/metadata \
    -v ${pwd}/sample/artifacts:/app/artifacts \
    -p 8010:8010 \
    s3rver:latest
```

## 👨‍💻 Dev

Run the following command to perform a development check on the server:

```bash
ruff check server
```

## 🌐 API

You can check the detailed API at `http://<host>:<port>/docs#/.`

Use the following commands to test the API endpoints:

```bash
# POST
http -f POST http://localhost:8010/api/v1/artifacts \
    file@README.md file@CHANGELOG.md version=1.0.0 tag=file

# GET
http -f GET http://localhost:8010/api/v1/artifacts/<hash_value>.README.md --download

# DELETE
http -f DELETE http://localhost:8010/api/v1/artifacts/<hash_value>.CHANGELOG.md
```
