from pydantic import BaseModel, Field


class Custom(BaseModel):
    version: str = Field(..., example="1.0.0")

class Hashes(BaseModel):
    sha256: str

class Info(BaseModel):
    custom: Custom
    hashes: Hashes
    length: int

class Artifact(BaseModel):
    info: Info
    path: str

class Artifacts(BaseModel):
    artifacts: list[Artifact]

class ResponseModel(BaseModel):
    detail: str
