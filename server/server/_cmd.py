import os
import sys
from pathlib import Path
from typing import Optional

import typer
import uvicorn

from .main import create_app
from .tuf import create_router_for_tuf

app = typer.Typer()


@app.command("run-server")
def cmd_run_server(
    artifact_dir: str = "artifacts",
    metadata_dir: Optional[str] = typer.Option(default=None),  # noqa: UP007 (Typer not yet supported)
    port: int = 8010,
    host: str = "127.0.0.1",
    database_url: str = "sqlite:///./test.db",
) -> None:
    Path(artifact_dir).mkdir(parents=True, exist_ok=True)

    database_url_env = os.environ.get("DATABASE_URL", None)
    if database_url_env is not None:
        database_url = database_url_env
    app_ = create_app(artifact_dir, database_url)

    if metadata_dir is not None:
        Path(metadata_dir).mkdir(parents=True, exist_ok=True)
        router_tuf = create_router_for_tuf(metadata_dir)
        app_.include_router(router_tuf, prefix="/api/v1")

    try:
        uvicorn.run(app_, host=host, port=port)
    except KeyboardInterrupt:
        sys.exit(0)
