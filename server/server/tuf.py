import re
from pathlib import Path

from fastapi import APIRouter, FastAPI, HTTPException, Request, status
from fastapi.responses import FileResponse

from .model import ResponseModel

_PATTERN = re.compile(r"(?P<version>(\d*))\.?(?P<role>(.+))")


def _extract_version_and_role(file_path: str) -> tuple[int, str] | None:
    """
    Extract the version and role from a given file path based on its stem.

    The function expects the file stem to contain a version number, followed
    by an optional period, and then the role. If no version number is present,
    it defaults to 0.

    Args:
        file_path (str): The path to the file from which to extract the version
            and role.

    Returns:
        Optional[tuple[int, str]]: A tuple containing the version as an integer
            and the role as a string, or None if the pattern does not match.
    """
    file_stem = Path(file_path).stem
    match = _PATTERN.match(file_stem)
    if match is None:
        return None
    version = match.group("version") or "0"
    role = match.group("role")
    return int(version), role


def create_router_for_tuf(metadata_dir: str | Path) -> FastAPI:

    metadata_dir = Path(metadata_dir)

    router_metadata = APIRouter()

    @router_metadata.get(
        "/metadata/{path_str:path}",
        description="This endpoint allows you to download metadata.",
        responses={
            200: {"description": "Successful Upload targets."},
            400: {"model": ResponseModel,
                  "description": "Bad Request - No version provided."},
            404: {"model": ResponseModel, "description": "Metadata file fot found"},
        })
    async def download_metadata(request: Request, path_str: str,  # noqa: ARG001
                                ) -> FileResponse:
        info = _extract_version_and_role(path_str)

        if info is None:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                detail=(
                    "The server expects a path in the format "
                    "'<version>.<role>.json' or 'timestamp.json'.",
                ),
            )

        ver, role_name = info

        if role_name == "timestamp":
            filename = f"{role_name}.json"
        else:
            filename = f"{ver}.{role_name}.json"

        path = metadata_dir / filename

        if not path.exists():
            raise HTTPException(status_code=404, detail="File not found")

        return FileResponse(path=path, media_type="application/json")

    return router_metadata
