import re
from collections.abc import Callable


class FileSizeExceededExceptionError(Exception):
    def __init__(
        self, current_part_name: str, current_part_size: int, max_allowed_size: int,
    ) -> None:
        self.current_part_size: int = current_part_size
        self.max_allowed_size: int = max_allowed_size
        super().__init__(
            f"{current_part_name} size ({self.current_part_size}) exceeds "
            f"the maximum allowed size ({self.max_allowed_size}).",
        )


class MultipartDataHandler:
    def __init__(self, max_allowed_size: int = 10**9) -> None:
        self.current_part_size: int = 0
        self.current_part_data: list[bytes] = []
        self.current_part_name: str = ""

        # Internal storage for parsed data
        self._parsed_data: dict[str, bytes] = {}

        self._is_version_set: bool = False
        self._version: str | None = None

        self._is_tag_set: bool = False
        self._tag: str | None = None

        self.max_allowed_size: int = max_allowed_size

    @property
    def parsed_data(self) -> dict[str, bytes]:
        return self._parsed_data

    @property
    def version(self) -> str | None:
        return self._version

    @property
    def tag(self) -> str | None:
        return self._tag

    def set_filename_from_header(self, header_data:bytes, start:int, end:int) -> None:
        data = header_data[start:end].decode()

        if "form-data" not in data:
            return

        match = re.search(r'filename="(?P<filename>[^"]*)"', data)
        if match:
            filename = match.group("filename")
            self.current_part_name = filename

        match = re.search(r'name="version"', data)
        if match:
            self._is_version_set = True

        match = re.search(r'name="tag"', data)
        if match:
            self._is_tag_set = True

    def initialize_part(self) -> None:
        self.current_part_size = 0
        self.current_part_data = []

    def handle_part_data(
        self, data_chunk: bytes, start_index: int, end_index: int,
    ) -> None:
        self.current_part_size += end_index - start_index

        if self.current_part_size > self.max_allowed_size:
            raise FileSizeExceededExceptionError(
                self.current_part_name, self.current_part_size, self.max_allowed_size,
            )

        self.current_part_data.append(data_chunk[start_index:end_index])

    def finalize_current_part(self) -> None:
        combined_part_data: bytes = b"".join(self.current_part_data)
        if len(self.current_part_name) != 0:
            self._parsed_data[self.current_part_name] = combined_part_data
        elif self._is_version_set:
            self._version = combined_part_data.decode()
        elif self._is_tag_set:
            self._tag = combined_part_data.decode()
        self._is_version_set = False
        self._is_tag_set = False
        self.current_part_size = 0
        self.current_part_data = []
        self.current_part_name = ""

    @property
    def multipart_callbacks(self) -> dict[str, Callable]:
        return {
            "on_header_value": self.set_filename_from_header,
            "on_part_begin": self.initialize_part,
            "on_part_data": self.handle_part_data,
            "on_part_end": self.finalize_current_part,
        }
