from pathlib import Path

from fastapi import APIRouter, FastAPI, HTTPException, Query, Request, status
from fastapi.responses import FileResponse, JSONResponse
from multipart.multipart import MultipartParser, parse_options_header
from sqlalchemy import and_
from sqlalchemy.exc import DatabaseError, IntegrityError
from sqlalchemy.orm import sessionmaker

from .database import (
    ArtifactInfo,
    ArtifactInfoModel,
    Base,
    wrapper_create_engine,
)
from .handler import FileSizeExceededExceptionError, MultipartDataHandler
from .model import Artifacts, ResponseModel
from .utils import AtomicFileWriter, digest_bytes


def create_app(  # noqa: C901 PLR0915
    artifact_dir: str | Path, db_url: str | Path,
) -> FastAPI:

    artifact_dir = Path(artifact_dir)

    engine = wrapper_create_engine(db_url)
    Base.metadata.create_all(bind=engine)

    session_local = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    db = session_local()

    app = FastAPI()

    router_v1 = APIRouter()

    # TENTATIVE
    @router_v1.get(
        "/artifacts/{filename}/hashes", summary="Get artifact's hash value",
        responses={
            200: {"description": "Successful get artifacts."},
            404: {"model": ResponseModel, "description": "Artifact file fot found"},
            })
    def get_artifacts_info(filename: str, tag: str = Query(None)) -> JSONResponse:
        try:
            if tag is None:
                _filter = and_(ArtifactInfo.filename == filename)
            else:
                _filter = and_(ArtifactInfo.filename == filename,
                               ArtifactInfo.tag == tag)
            db_ti_list = db.query(ArtifactInfo).filter(_filter).all()
            if len(db_ti_list) == 0:
                raise HTTPException(status_code=404, detail="no data")
            infos = {db_ti.version: db_ti.hash_value
                     for db_ti in db_ti_list}

        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e
        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content={"hashes": infos})  # Need algorithm: DEFAULT_HASH_ALGORITHM ?

    @router_v1.get(
        "/artifacts/{filename_with_hash}", summary="Dwonload artifacts",
        responses={
            200: {"description": "Successful get artifacts."},
            404: {"model": ResponseModel, "description": "Artifact file fot found"},
            })
    def download_artifacts(filename_with_hash: str) -> FileResponse:
        path = artifact_dir / filename_with_hash
        if not path.exists():
            raise HTTPException(status_code=404, detail="no data")

        return FileResponse(
            path=path, media_type="application/octet-stream",
            filename=filename_with_hash,
        )

    @router_v1.post(
        "/artifacts", summary="Upload artifacts",
        description="This endpoint allows you to upload artifacts.",
        responses={
            200: {"model": Artifacts, "description": "Successful Upload artifacts."},
            415: {"model": ResponseModel,
                  "description": "Unsupported MediaType. No file provided."},
            413: {"model": ResponseModel, "description": "File size exceeded."},
            400: {"model": ResponseModel,
                  "description": "Bad Request - No version provided."},
            409: {"model": ResponseModel,
                  "description":
                      "Conflict - Artifact already exists or Conflict hash."},
            500: {"model": ResponseModel,
                  "description": "Something happened :("},
    })
    async def upload_artifacts(request: Request) -> JSONResponse:  # noqa: C901
        content_type, params = parse_options_header(request.headers["content-type"])
        params["boundary"] = params.get(b"boundary")

        if content_type != b"multipart/form-data":
            raise HTTPException(
                status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                detail=(
                    f"Unsupported MediaType: {content_type.decode()}. ",
                    "Use 'multipart/form-data'.",
                ),
            )

        if params["boundary"] is None:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                detail="Missing boundary in multipart/form-data",
            )

        handler = MultipartDataHandler()
        parser = MultipartParser(params["boundary"], handler.multipart_callbacks)

        async for chunk in request.stream():
            try:
                parser.write(chunk)
            except FileSizeExceededExceptionError as e:
                raise HTTPException(status_code=413, detail=str(e)) from e

        artifacts_version = handler.version
        if artifacts_version is None:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Please provide version= as form data.")

        artifacts_tag = handler.tag
        if artifacts_tag is None:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Please provide tag= as form data.")

        artifacts_info = []
        for _filename, _data in handler.parsed_data.items():
            _hash_value = digest_bytes(_data).hexdigest()

            ti = ArtifactInfoModel(filename=_filename, version=artifacts_version,
                                 tag=artifacts_tag,
                                 hash_value=_hash_value, size=len(_data))

            if (artifact_dir / ti.filename_w_hash).exists():
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail=f"{_filename} is arleady exists or Conflict hash value.")

            artifacts_info.append((ti, _data))

        artifacts = []
        for ti, data in artifacts_info:
            _db_ti = ArtifactInfo(**ti.model_dump())
            _path = artifact_dir / ti.filename_w_hash
            try:
                db.add(_db_ti)
                with AtomicFileWriter(_path) as f:
                    f.write(data)
                artifacts.append(ti.file_info)
            except (OSError, MemoryError) as e:
                db.rollback()
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                    detail="Something happened") from e
        try:
            db.commit()
        except IntegrityError as e:
            db.rollback()
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=f"Couldn't add {ti.filename} to DB.") from e

        return JSONResponse(
                status_code=status.HTTP_200_OK,
                content={"artifacts": artifacts})

    @router_v1.delete(
        "/artifacts/{filename_with_hash}", summary="Delete artifacts",
        description="This endpoint allows you to delete artifacts.",
        responses={
            200: {"model": ResponseModel,
                  "description": "Successful delete artifacts."},
            400: {"model": ResponseModel,
                  "description": "Invalid filename format."},
            404: {"model": ResponseModel, "description": "Artifact file fot found"},
            500: {"model": ResponseModel, "description": "Database Error"},
            })
    async def delete_artifact(filename_with_hash: str) -> JSONResponse:
        path = artifact_dir / filename_with_hash

        if not path.exists():
            raise HTTPException(status_code=404, detail="File not found")

        try:
            hash_value, filename = str(filename_with_hash).split(".", maxsplit=1)
        except ValueError as e:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Invalid path format") from e

        try:
            db_ti = db.query(ArtifactInfo).filter(
                and_(ArtifactInfo.filename == filename,
                     ArtifactInfo.hash_value == hash_value),
                ).first()
            if db_ti is not None:
                db.delete(db_ti)
        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e

        path.unlink()
        db.commit()

        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content={"detail": f"{filename_with_hash} is deleted."})

    app.include_router(router_v1, prefix="/api/v1")

    return app
