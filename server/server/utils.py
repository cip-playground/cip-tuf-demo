import os
import tempfile
from io import BufferedIOBase
from pathlib import Path
from types import TracebackType

from securesystemslib.hash import (
    DEFAULT_HASH_ALGORITHM,
    DEFAULT_HASH_LIBRARY,
    PycaDiggestWrapper,
    digest,
)


def digest_bytes(
    data_bytes: bytes,
    algorithm: str = DEFAULT_HASH_ALGORITHM,
    hash_library: str = DEFAULT_HASH_LIBRARY,
) -> PycaDiggestWrapper:

    digest_object = digest(algorithm, hash_library)
    digest_object.update(data_bytes)

    return digest_object


class AtomicFileWriter:
    """
    A context manager for atomic file writing. This class ensures that the file
    is only saved if the entire block completes successfully, preventing
    partial writes.

    Usage:
        with AtomicFileWriter('foo.txt') as file:
            file.write(b'Hello world.')

    Attributes:
        artifact_path (Path): The final path where the written file will be stored.
        temp_file_path (Path): The temporary file path used during writing.
    """

    def __init__(self, artifact_path: str | Path) -> None:
        """
        Initialize the AtomicFileWriter with the artifact path.

        Args:
            artifact_path (Union[str, Path]): The path to the file that should
                be written atomically. Can be a string or a Path object.
        """
        if isinstance(artifact_path, str):
            artifact_path = Path(artifact_path)
        self.artifact_path: Path = artifact_path
        self.temp_file_path: Path

    def __enter__(self) -> BufferedIOBase:
        """
        Enter the runtime context and create a temporary file for writing.

        Returns:
            BufferedIOBase: A file object that can be used to write to the file.
        """
        temp_file_descriptor, temp_file_path = tempfile.mkstemp(
            dir=self.artifact_path.parent,
        )
        self.temp_file_path = Path(temp_file_path)
        self.file = os.fdopen(temp_file_descriptor, "wb")
        return self.file

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> None:
        """
        Exit the runtime context and replace the artifact file with the temporary
        file if no exception occurred. Otherwise, remove the temporary file.

        Args:
            exc_type (Optional[type]): The type of the exception, if any.
            exc_value (Optional[Exception]): The exception instance, if any.
            traceback (Optional[TracebackType]): The traceback object, if any.
        """
        if exc_type is None:
            self.temp_file_path.replace(self.artifact_path)
        if self.file is not None:
            self.file.close()
        if self.temp_file_path.exists():
            self.temp_file_path.unlink()

        if exc_type is not None:
            raise exc_value
