from logging import getLogger

from pydantic import BaseModel
from securesystemslib.hash import DEFAULT_HASH_ALGORITHM
from sqlalchemy import Column, Integer, String, UniqueConstraint, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import declarative_base

logger = getLogger(__name__)
logger.info("message")

def wrapper_create_engine(url: str) -> Engine:
    db_name = url.split(":")[0]
    connect_args = {"check_same_thread": False} if db_name == "sqlite" else {}
    return create_engine(url, connect_args=connect_args)

Base = declarative_base()

class ArtifactInfo(Base):
    __tablename__ = "artifacts"

    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String, nullable=False)
    version = Column(String)
    tag = Column(String)
    hash_value = Column(String, nullable=False, unique=True)
    size = Column(Integer)

    __table_args__ = (
        UniqueConstraint("filename", "version", name="_filename_version_uc"),)

class ArtifactInfoModel(BaseModel):
    filename: str
    version: str
    tag: str
    hash_value: str
    size: int

    @property
    def filename_w_hash(self) -> str:
        return f"{self.hash_value}.{self.filename}"

    @property
    def file_info(self) -> None:
        file_info = {
            "length": self.size,
            "hashes": {DEFAULT_HASH_ALGORITHM: self.hash_value},
            "custom": {"version": self.version}}
        return {"path": self.filename, "info": file_info}
