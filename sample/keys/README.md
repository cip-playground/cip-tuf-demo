# README.md

There are one online keys, and the password for each is `1234`.

The key information can be checked by using the `rstuf key info` command.

```bash
rstuf key info
# Prints:
# Choose key type [ed25519/ecdsa/rsa] (ed25519): 
# Enter the private key path: sample/keys/online
# Enter the private key password: 
# ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
# ┃                              Key ID                              ┃ Key Type ┃ Key Scheme ┃                            Public Key                            ┃
# ┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
# │ 365a563b5bb296f386f7d729ca8860e6b01b860f511c7316d37116f8dd43898b │ ed25519  │  ed25519   │ 9c7a3881c559ecbc48d911e44acb86d7a943b732e45fc6e4b671ffff255a20ca │
# └──────────────────────────────────────────────────────────────────┴──────────┴────────────┴──────────────────────────────────────────────────────────────────┘
```
