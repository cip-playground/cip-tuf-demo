#!/bin/sh

set -e

docker create --name tmp cip-tuf-demo-metadata-key:latest
docker cp tmp:/hermit_crab/metadata/root/1.root.json ~/root.json
docker rm -f tmp
