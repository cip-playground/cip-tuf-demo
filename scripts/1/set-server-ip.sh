#!/bin/sh

if [ -z "${1}" ]; then
	echo "Usage: ${0} <server IP>"
	exit 1
fi
sed -i "s@XXX\.XXX\.XXX\.XXX@${1}@g" \
	app/app/components/software/index.tsx \
	app/app/components/delete/index.tsx \
	app/app/components/upload/index.tsx \
	isar-cip-core-tuf/recipes-core/swupdate-config/files/swupdate.cfg
