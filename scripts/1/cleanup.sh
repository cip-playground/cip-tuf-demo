#!/bin/sh
# Use this script when you restart from the step 1 in README.md

set -e

if ! echo "${0}" | grep -q "scripts/1/cleanup.sh"; then
	echo "Please run from the top directory i.e. ./scripts/1/cleanup.sh"
	exit 1
fi

sudo rm -rf isar-cip-core/build*
rm -rf isar-cip-core/isar-cip-core-tuf

docker image rm cip-tuf-demo-target
docker image rm cip-tuf-demo-metadata-key
docker image rm cip-tuf-demo-wfx
docker image rm cip-tuf-demo-app

# Revert chanages by set-server-ip.sh
git checkout .

echo "Done"
