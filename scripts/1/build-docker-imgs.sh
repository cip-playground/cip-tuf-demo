#!/bin/sh

set -e

echo "===== Target manager ====="
cd tuf/fileserver
docker build -t cip-tuf-demo-target .
cd ../..

echo "===== Metadata & key manager ====="
cd tuf/hermit_crab
docker build -t cip-tuf-demo-metadata-key .
cd ../..

echo "===== wfx ====="
cd wfx
docker build -t cip-tuf-demo-wfx .
cd ..

echo "===== Demo application ====="
cd app
docker build -t cip-tuf-demo-app .
cd ..

echo "Done"
