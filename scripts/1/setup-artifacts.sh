#!/bin/sh

set -e

ARTIFACT_DIR=artifacts
DOCKER_IMGS="target metadata-key wfx app"

rm -rf ${ARTIFACT_DIR} ${ARTIFACT_DIR}.tar.gz
mkdir -p ${ARTIFACT_DIR}

echo "Copying artifacts"

mkdir -p ${ARTIFACT_DIR}/docker
for img in ${DOCKER_IMGS}; do
	docker save cip-tuf-demo-${img} | gzip -c > ${ARTIFACT_DIR}/docker/cip-tuf-demo-${img}.tar.gz
done

cp -rL swu scripts ${ARTIFACT_DIR}

# QEMU
ISAR_CIP_CORE_DEPLOY_DIR=isar-cip-core/build/tmp/deploy/images/qemu-amd64
mkdir -p ${ARTIFACT_DIR}/${ISAR_CIP_CORE_DEPLOY_DIR}/OVMF
install -m 0644 \
	${ISAR_CIP_CORE_DEPLOY_DIR}/cip-core-image-cip-core-bookworm-qemu-amd64.wic \
	${ARTIFACT_DIR}/${ISAR_CIP_CORE_DEPLOY_DIR}
install -m 0644 \
	${ISAR_CIP_CORE_DEPLOY_DIR}/OVMF/OVMF_CODE_4M.fd \
	${ARTIFACT_DIR}/${ISAR_CIP_CORE_DEPLOY_DIR}/OVMF
install -m 0755 isar-cip-core/start-qemu.sh ${ARTIFACT_DIR}/isar-cip-core

# BBB
cat isar-cip-core/build-bbb-1.0.0/tmp/deploy/images/bbb/cip-core-image-cip-core-bookworm-bbb.wic | \
	gzip > ${ARTIFACT_DIR}/bbb.wic.gz
cp isar-cip-core/build-bbb-1.0.0/tmp/deploy/images/bbb/cip-core-image-cip-core-bookworm-bbb.wic.bmap \
	${ARTIFACT_DIR}/bbb.wic.bmap

echo "Compressing artifacts"
tar czf ${ARTIFACT_DIR}.tar.gz ${ARTIFACT_DIR}
rm -r ${ARTIFACT_DIR}

echo "Done"
