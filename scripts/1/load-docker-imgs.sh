#!/bin/sh

set -e

DOCKER_DIR=docker
DOCKER_IMGS="target metadata-key wfx app"

for img in ${DOCKER_IMGS}; do
	echo "Loading cip-tuf-demo-${img}"
	zcat ${DOCKER_DIR}/cip-tuf-demo-${img}.tar.gz | docker load
done
echo "Done"
