import gzip
import os
import shutil
import tempfile
from base64 import b64encode
from pathlib import Path
from types import TracebackType
from typing import NamedTuple, Optional

import libarchive
import typer
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, padding, serialization
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.serialization import pkcs7
from cryptography.hazmat.primitives.serialization.pkcs7 import PKCS7Options
from jinja2 import Environment, FileSystemLoader
from rdiff_backup import librsync
from typing_extensions import Annotated  # noqa: UP035


class _CMP:
    def __enter__(self) -> "_CMP":
        return self
    def __exit__(self,
                 exc_type: None | type[BaseException],
                 exc_value: None | BaseException,
                 traceback: None | TracebackType) -> None:
        self.close()

class _SF(librsync.SigFile, _CMP):
    pass

class _DF(librsync.DeltaFile, _CMP):
    pass


class Image(NamedTuple):
    filename: str
    device: str
    ivt: str | None
    sha256: str
    filesystem: str
    subtype: str
    path_to_file: str  # not used in sw-description

    type: str = "roundrobin"
    path: str = ""
    compressed: str = ""
    properties: None | dict = None

    encrypted: bool = False
    include_installed_directly: bool = False


def _do_ungzip(gz: Path) -> None:
    p_out = gz.with_suffix("")
    with gzip.open(gz, "rb") as f_in, p_out.open(mode="wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    return p_out

def _do_gzip(unzipped: Path) -> Path:
    p_out = unzipped.with_suffix(unzipped.suffix + ".gz")
    with unzipped.open(mode="rb") as f_in, gzip.open(p_out, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    return p_out

def create_delta(rootfs_v1: Path, rootfs_v2: Path, delta: Path) -> None:
    with _SF(rootfs_v1.open("rb")) as sf, _DF(sf.read(), rootfs_v2.open("rb")) as df:
        delta.write_bytes(df.read())

def _read_key_and_iv(keypath: str) -> tuple[bytes, bytes]:
    with Path(keypath).open(mode="r") as f:
        key_iv = f.read().strip()

    # putting the key and iv hex values on one line separated by whitespace
    key, iv = key_iv.split(maxsplit=1)
    return  bytes.fromhex(key), bytes.fromhex(iv)

def enc_aes_256_cbc(key: bytes, iv: bytes, data: bytes,
                    *, base64 : bool = False) -> bytes:

    padder = padding.PKCS7(algorithms.AES.block_size).padder()
    padded_data = padder.update(data)
    padded_data += padder.finalize()

    cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
    encryptor = cipher.encryptor()
    ct = encryptor.update(padded_data)
    ct += encryptor.finalize()

    return b64encode(ct) if base64 else ct

def sha256sum(data: bytes) -> str:
    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(data)
    return digest.finalize().hex()

def enc(data_path: Path, output_path: Path, swu_key_path: Path) -> tuple[bytes, str]:
    data = data_path.read_bytes()

    sym_key, _ = _read_key_and_iv(swu_key_path)
    iv = os.urandom(16)
    ct = enc_aes_256_cbc(sym_key, iv, data)

    output_path.write_bytes(ct)

    return iv, sha256sum(ct)

def make_sw_description(    # noqa: PLR0913
    name: str, os_version: str, images: list[Image], files: list[Image],
    filepath: Path, hw_compatibility: str) -> None:

    loader = FileSystemLoader(Path(__file__).parent)
    template = Environment(  # noqa: S701 sw-description is not HTML
        loader=loader).get_template(name="template.jinja2")
    with filepath.open("w") as f:
        description = template.render(
            name=name, os_version=os_version, images = images, files = files,
            hw_compatibility=hw_compatibility)
        f.write(description)

def sign(cert_pem_path: Path, key_pem_path: Path, sw_description_path: Path,
         sign_suffix: str = "sig") -> Path:
    signer_cert = x509.load_pem_x509_certificate(
        cert_pem_path.read_bytes(), backend=default_backend())
    signer_key = serialization.load_pem_private_key(
        key_pem_path.read_bytes(), password=None, backend=default_backend())
    data_to_sign = sw_description_path.read_bytes()

    signed_data = pkcs7.PKCS7SignatureBuilder().set_data(data_to_sign).add_signer(
        signer_cert, signer_key, hashes.SHA256(),
    ).sign(serialization.Encoding.DER,
           options=[PKCS7Options.NoAttributes, PKCS7Options.Binary])

    sign_path = Path(f"{sw_description_path}.{sign_suffix}")
    sign_path.write_bytes(signed_data)

    return sign_path

def create_cpio_archive(cpio: Path, files: list[str]) -> None:
    original_directory = Path().cwd()
    paths = [Path(file) for file in files]
    try:
        with tempfile.TemporaryDirectory() as tempdir:
            os.chdir(tempdir)

            for path in paths:
                absolute_path = original_directory / path
                shutil.copy(absolute_path, Path(tempdir) / path.name)

            with libarchive.file_writer(cpio.name, "cpio_newc")as archive:
                for path in paths:
                    archive.add_file(path.name)
            shutil.copy(cpio.name, original_directory / cpio)

    finally:
        os.chdir(original_directory)

def process_file(  # noqa: PLR0913
    file_path: Path,  swupdate_key_path: str, image_type: str, mapping: str,
    properties: dict,
    *, encrypt: bool, compressed: str | None = None) -> Image:

    filename_encrypted = None
    file_enc = None
    if encrypt:
        file_enc = file_path.with_suffix(file_path.suffix + ".enc")
        iv, hash_value = enc(file_path, file_enc, swupdate_key_path)
        iv = iv.hex()
        filename_encrypted = file_enc.name
    else:
        iv = None
        hash_value = sha256sum(file_path.read_bytes())

    return Image(
        filename=filename_encrypted or file_path.name,
        device=mapping,
        ivt=iv,
        sha256=hash_value,
        filesystem="vfat" if image_type == "uki" else "ext4",
        subtype="kernel" if image_type == "uki" else "image",
        path_to_file=file_enc or file_path,
        path=file_path.name,
        compressed=compressed,
        encrypted=encrypt, properties=properties,
    )

# Command line interface

app = typer.Typer()

@app.command()
def create(  # noqa: PLR0913
    uki: Annotated[
        Optional[Path], typer.Option(help="path to the uki file")] = None,  # noqa: UP007 (Typer not yet supported)
    rootfs: Annotated[
        Optional[Path], typer.Option(help="path to the rootfs file")] = None,  # noqa: UP007
    rootfs_previous: Annotated[
        Optional[Path], typer.Option(help="path to the previous rootfs file")] = None,  # noqa: UP007
    os_version: Annotated[
        str, typer.Option(help="OS version")] = ...,
    swupdate_key_path: Annotated[
        Optional[Path], typer.Option(help="path to the swupdate key")] \
            = "swupdate.sym.key",  # noqa: UP007
    uuid_0: Annotated[
        str, typer.Option(help="UUID of the A(0) partition")] = "ROOTFS0",
    uuid_1: Annotated[
        str, typer.Option(help="UUID of the B(1) partition")] = "ROOTFS1",
    compressed_rootfs: Annotated[
        str, typer.Option(help="Compression algorithm for the rootfs image")]="zlib",
    uki_filename: Annotated[
        Optional[str], typer.Option(help="Name of the uki file")] = None,  # noqa: UP007
    sw_description: Annotated[
        Path, typer.Option(help="Name of the sw-description file")] \
            = Path("sw-description"),
    sing_suffix: Annotated[
        str, typer.Option(help="Suffix for the signature file")] = "sig",
    swu_cert: Annotated[
        Path, typer.Option(help="Name of the swupdate certificate")] \
            = Path("swupdate.cert.pem"),
    swu_key: Annotated[
        Path, typer.Option(help="Name of the swupdate key")] = Path("swupdate.key.pem"),
    update_cpio: Annotated[
        Path, typer.Option(help="Name of the update cpio archive")] = "update.swu",
    properties: Annotated[
        Optional[list[str]], typer.Option(help="Properties for the image")] = None,  # noqa: UP007
    *,
    encrypt: Annotated[
        bool, typer.Option(help="Encrypt the image if set to True")] = False,
    hw_compatibility: Annotated[
        str, typer.Option(help="Hardware compatibility")] = "1.0",
    ) -> None:

    if properties is None:
        properties = []

    property_dict = {}
    for p in properties:
        key, value = p.split("=", maxsplit=1)
        property_dict[key] = value

    files, images = [], []
    uki_filename = uki_filename or (uki.name if uki is not None else None)

    if uki is not None:
        mapping = f"C:BOOT0:{uki_filename}->BOOT0,C:BOOT1:{uki_filename}->BOOT1"
        uki_image = process_file(uki, swupdate_key_path, "uki", mapping, property_dict,
                                 encrypt=encrypt)
        files.append(uki_image)

    if rootfs is not None:
        mapping = f"C:BOOT0:{uki_filename}->{uuid_0},C:BOOT1:{uki_filename}->{uuid_1}"
        image = rootfs
        if rootfs_previous is not None:
            property_dict["chainhandler"] = "rdiff_image"

            rootfs_ungzip = _do_ungzip(rootfs)
            rootfs_previous_ungzip = _do_ungzip(rootfs_previous)

            delta_rootfs = rootfs_ungzip.parent / "delta" / rootfs_ungzip.name
            delta_rootfs.parent.mkdir(exist_ok=True)

            create_delta(rootfs_previous_ungzip, rootfs_ungzip, delta_rootfs)
            image = _do_gzip(delta_rootfs)

        rootfs_image = process_file(
            image, swupdate_key_path, "rootfs", mapping, property_dict,
            compressed=compressed_rootfs, encrypt=encrypt)
        images.append(rootfs_image)

    make_sw_description(
        name="OS update for QEMU",
        os_version=os_version,
        images=images,
        files=files,
        filepath=sw_description,
        hw_compatibility=hw_compatibility)

    sign_path = sign(swu_cert, swu_key, sw_description, sign_suffix=sing_suffix)

    targets = [sw_description, sign_path]
    targets = [*targets,
               *[image.path_to_file for image in images],
               *[file.path_to_file for file in files]]
    create_cpio_archive(update_cpio, targets)

if __name__ == "__main__":
    typer.run(create)
