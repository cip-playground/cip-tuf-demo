from pathlib import Path

from .base import BaseAPI, File, HttpRequestInfo, action


class ServerAPI(BaseAPI):

    @action("GET")
    def get_versions(self, filename: str) -> HttpRequestInfo:
        return HttpRequestInfo(route=("artifacts", filename, "hashes"))

    @action("GET")
    def get_file(self, filename_w_hash: str) -> HttpRequestInfo:
        return HttpRequestInfo(route=("artifacts", filename_w_hash))

    @action("POST")
    def post_file(self, filename: str, formdata: dict) -> HttpRequestInfo:
        path = Path(filename)
        with path.open(mode="rb") as f:
            binary_data = f.read()
        files = File(filename=path.name, binary_data=binary_data)
        return HttpRequestInfo(route="artifacts", formdata=formdata, files=files)

    @action("DELETE")
    def delete_file(self, filena_w_hash: str) -> None:
        return HttpRequestInfo(route=("artifacts", filena_w_hash))
