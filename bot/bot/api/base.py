import functools
import json
from logging import getLogger
from pathlib import Path
from urllib.parse import urljoin

import requests
from pydantic import BaseModel, Field

logger = getLogger(__name__)


class File(BaseModel):
    filename: str | Path
    binary_data: bytes

    def convert_to_upload_format(self) -> dict[str, tuple[str, bytes]]:
        if isinstance(self.filename, Path):
            self.filename = str(self.filename)
        return {"file": (self.filename, self.binary_data, "multipart/form-data")}

class HttpRequestInfo(BaseModel):
    route: str | tuple[str, ...]
    formdata: dict | None = Field(None)
    files: File | None = Field(None)
    query: dict | None = Field(None)

    @property
    def route_(self) -> tuple[str, ...]:
        if isinstance(self.route, str):
            self.route = tuple((self.route,))  # noqa: C409
        return self.route

def _get_data(url: str, query: dict, timeout: int = 10) -> dict | None | bytes:
    response = requests.get(url, params=query, timeout=timeout)
    logger.debug(response.status_code)
    if response.status_code != requests.codes.ok:
        return None
    headers = response.headers

    content_type = headers.get("content-type")
    if content_type is None:
        return None

    if content_type == "application/json":
        return response.json()

    if content_type == "application/octet-stream":
        data = response.content
        if data is None:
            return None
        return data

    return None

def _post_data(url: str,
               formdata: dict,
               query: dict | None = None,
               files: File | None = None, timeout: int = 10,
               ) -> dict:

    if files is None:
        headers = {"Content-Type": "application/json"}
        data = json.dumps(formdata)
    else:
        # Do not use multipart/form-data directly
        #   because the boundary is not set correctly.
        headers = None
        files = files.convert_to_upload_format()
        data = formdata

    response = requests.post(url, files=files, data=data,
                             headers=headers, timeout=timeout, params=query)
    logger.info("status_code is %s", response.status_code)
    ok_status = [requests.codes.created, requests.codes.ok, requests.codes.accepted]
    if response.status_code not in ok_status:
        logger.info("Retry with formdata and headers=None")
        response = requests.post(
            url, files=files, data=formdata,
            headers=None, timeout=timeout, params=query)
        logger.info("status_code is %s", response.status_code)
        if response.status_code not in ok_status:
            return None

    return response.json()

def _put_data(url: str, formdata: dict, query: dict | None = None, timeout: int = 10,
               ) -> dict:
    if "wfx" in url:
        headers = {"Content-Type": "application/json", "accept": "application/json"}
        data = json.dumps(formdata)
    else:
        headers = None
        data = formdata
    response = requests.put(url, data=data, headers=headers,
                             timeout=timeout, params=query)
    if response.status_code != requests.codes.ok:
        return None
    return response.json()

def _delete_data(url: str, query: dict | None = None, timeout: int = 10) -> None:
    headers = {"Content-Type": "application/json"}
    response = requests.delete(url, headers=headers, params=query, timeout=timeout)
    if response.status_code not in [requests.codes.no_content, requests.codes.ok]:
        return None
    return response.content

def action(method: str, return_class: BaseModel | None = None, timeout :int=10,
           ) -> callable:

    def decoration(func: callable) -> callable:

        @functools.wraps(func)
        def infunc(*args, **kwargs) -> BaseModel | None:  # noqa: ANN002 ANN003
            hr_info  = func(*args, **kwargs)

            if not isinstance(hr_info, HttpRequestInfo):
                msg = (
                    f"Function {func.__name__} expected to return "
                    f"an instance of {HttpRequestInfo}, "
                    f"but got {type(hr_info)}")
                logger.error(msg)
                raise TypeError

            # args[0] expects an instance of the Class(BaseAPI)
            url = args[0].create_full_url(*hr_info.route_)

            try:
                m = method.upper()
                if   m == "GET":
                    data = _get_data(
                        url,
                        query=hr_info.query,
                        timeout=timeout)
                elif m == "POST":
                    data = _post_data(
                        url,
                        query=hr_info.query,
                        formdata=hr_info.formdata,
                        files=hr_info.files,
                        timeout=timeout)
                elif m == "PUT":
                    data = _put_data(
                        url,
                        query=hr_info.query,
                        formdata=hr_info.formdata,
                        timeout=timeout)
                elif m == "DELETE":
                    return _delete_data(
                        url,
                        query=hr_info.query,
                        timeout=timeout)

            except requests.exceptions.ConnectionError:
                logger.exception(
                    "Connection error occurred while making the request")
                return None

            if data is None:
                return None

            return return_class(**data) if return_class is not None else data

        return infunc

    return decoration

class BaseAPI:
    def __init__(self, base_url: str) -> None:
        self.base_url = base_url

    def create_full_url(self, *args: str) -> str:
        return urljoin(self.base_url, "/".join(args))
