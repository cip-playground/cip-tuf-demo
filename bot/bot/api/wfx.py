from enum import Enum

from pydantic import BaseModel, Field

from .base import BaseAPI, HttpRequestInfo, action


class State(str, Enum):
    NEW = "NEW"
    PROGRESS = "PROGRESS"
    DONE = "DONE"
    SUCCESS = "SUCCESS"
    FAILED = "FAILED"


class Status(BaseModel):
    client_id: str | None = Field(None, alias="clientId")
    definition_hash: str = Field(..., alias="definitionHash")
    state: str

class Content(BaseModel):
    client_id: str = Field(..., alias="clientId")
    definition: dict
    job_id: str = Field(..., alias="id")
    mtime: str
    stime: str
    status: Status
    workflow: dict
    history: list | None = Field(None)
    tags: list | None = Field(None)

class PaginatedList(BaseModel):
    content: list[Content]
    pagination: dict

class SouthboundAPI(BaseAPI):

    @action("GET", PaginatedList)
    def get_jobs(self, *, client_id: str | None = None, offset : int=0, limit: int=10,  # noqa: PLR0913
             sort: str="asc", state: str | None = None, group: str | None = None,
             workflow: str | None = None) -> str:
        query = {
            "clientId": client_id,
            "offset": offset,
            "limit": limit,
            "sort": sort,
            "state": state,
            "group": group,
            "workfolow": workflow,
        }
        return HttpRequestInfo(route="jobs", query=query)

    @action("GET", Content)
    def get_jobs_using_id(self, job_id: str) -> str:
        return HttpRequestInfo(route=("jobs", job_id))

    @action("PUT", Status)
    def put_jobs_sutatus_using_id(self, job_id: str, state: str) -> None:
        data = {"state": state}
        return HttpRequestInfo(route=("jobs", job_id, "status"), formdata=data)


class NorthboundAPI(SouthboundAPI):

    @action("POST", Content)
    def post_jobs(self, client_id: str, workflow: str,
                  user_defined: dict | None = None, tags: list | None = None) -> None:
        if tags is None:
            tags = []
        if user_defined is None:
            user_defined = {}

        data = {"clientId": client_id, "definition": {"userDefined": user_defined},
                "tags": tags, "workflow": workflow}
        return HttpRequestInfo(route="jobs", formdata=data)

    @action("POST")
    def post_workflow(self, workflow: dict) -> None:
        return HttpRequestInfo(route="workflows", formdata=workflow)

    @action("DELETE")
    def delete_jobs_using_id(self, job_id:str) -> None:
        return HttpRequestInfo(route=("jobs", job_id))
