from .base import BaseAPI, HttpRequestInfo, action


class RSTUFAPI(BaseAPI):

    @action("POST")
    def post_targets(self, target_json: dict) -> HttpRequestInfo:
        return HttpRequestInfo(route=("artifacts"), formdata=target_json)

    @action("POST")
    def delete_targets(self, targets: list) -> HttpRequestInfo:
        return HttpRequestInfo(route=("artifacts", "delete"),
                               formdata={"artifacts": targets})
