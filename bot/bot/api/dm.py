from pydantic import BaseModel

from .base import BaseAPI, HttpRequestInfo, action


class DeviceInfo(BaseModel):
    rootfs_version: str
    uki_version: str
    tag: str
    os_version: str
    status: int
    last_updated: str

class DeviceManagementAPI(BaseAPI):

    @action("GET", DeviceInfo)
    def get_device(self, device_id: str, tag: str | None = None) -> HttpRequestInfo:
        query = None if tag is None else {"tag": tag}
        return HttpRequestInfo(route=("devices", device_id), query=query)

    @action("GET")
    def get_devices(self) -> HttpRequestInfo:
        return HttpRequestInfo(route=("devices"))

    @action("PUT")
    def put_device(self, device_id: str,
                   rootfs_version: str | None = None,
                   uki_version: str | None = None,
                   os_version: str | None = None) -> HttpRequestInfo:
        data = {"device_id": device_id}
        if rootfs_version is not None:
            data["rootfs_version"] = rootfs_version
        if uki_version is not None:
            data["uki_version"] = uki_version
        if os_version is not None:
            data["os_version"] = os_version
        return HttpRequestInfo(route=("devices"), formdata=data)

    @action("POST")
    def add_device(self, device_id: str,
                   rootfs_version: str | None = None,
                   uki_version: str | None = None,
                   os_version: str | None = None,
                   group: str | None = None) -> HttpRequestInfo:
        data = {"device_id": device_id, "rootfs_version": rootfs_version,
                "uki_version": uki_version, "tag": group, "os_version": os_version}
        return HttpRequestInfo(route=("devices/"), formdata=data)

    @action("DELETE")
    def delete_device(self, device_id:str) -> HttpRequestInfo:
        return HttpRequestInfo(route=("devices", device_id))
