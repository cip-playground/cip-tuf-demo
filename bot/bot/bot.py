import time
from enum import Enum
from pathlib import Path
from typing import NamedTuple

import semver

from .api import RSTUFAPI, DeviceManagementAPI, NorthboundAPI, ServerAPI, SouthboundAPI
from .api.dm import DeviceInfo
from .api.wfx import State
from .create_swu import create

ROOTFS_VERSION = "rootfs_version"
UKI_VERSION = "uki_version"

base_dir = Path(__file__).resolve().parent
swu_scripts = base_dir / "create_swu.sh"

class APIUrls(NamedTuple):
    device_management: str
    server_internal: str
    server_external: str
    rstuf: str
    wfx_south: str
    wfx_north: str

class Keys(NamedTuple):
    sym_key: str
    cert_pem: str
    key_pem: str

class UpdateInfo(NamedTuple):
    os_version: str | None
    rootfs_version: str | None
    uki_version: str | None
    group: str | None

class VersionType(Enum):
    UPDATE = "update"
    CURRENT = "current"
    def __str__(self) -> str:
        return self.value

class Bot:
    def __init__(self, api_urls: APIUrls,
                 rootfs_filename: str = "rootfs.img.gz",
                 uki_filename: str = "unified-linux.efi") -> None:
        self.api_urls = api_urls
        self._api_dm = DeviceManagementAPI(self.api_urls.device_management)
        self._api_server_in = ServerAPI(self.api_urls.server_internal)
        self._api_server_ex = ServerAPI(self.api_urls.server_external)
        self._api_rstuf = RSTUFAPI(self.api_urls.rstuf)
        self._api_wfx_south = SouthboundAPI(api_urls.wfx_south)
        self._api_wfx_north = NorthboundAPI(api_urls.wfx_north)
        self.rootfs_filename = rootfs_filename
        self.uki_filename = uki_filename

    def get_device_info(self, device_id: str, tag: str | None = None,
                         ) -> DeviceInfo | None:
        return self._api_dm.get_device(device_id, tag)

    def get_file_versions(self, filename: str) -> dict | None:
        files = self._api_server_ex.get_versions(filename) or {}
        files.update(self._api_server_in.get_versions(filename) or {})
        return files

    def _select_update_files(self, device_id: str, tag: str,
                             update_info: UpdateInfo,
                             *, delta_update: bool = False) -> list:
        update_files = {VersionType.UPDATE: {}, VersionType.CURRENT: {}}

        device_info = self.get_device_info(device_id, tag)
        if device_info is None:
            return update_files

        update_versions = (update_info.rootfs_version, update_info.uki_version)
        device_versions = (device_info.rootfs_version, device_info.uki_version)

        for filename, update_version, device_version in zip(
            (self.rootfs_filename, self.uki_filename), update_versions, device_versions,
            strict=True):
            if semver.compare(update_version, device_version) <= 0:
                continue

            versions = self._api_server_in.get_versions(filename)
            try:
                hashes = versions.get("hashes")
                img_hash = hashes.get(update_version)
                if delta_update:
                    img_hash_device = hashes.get(device_version)

            except AttributeError:
                continue

            update_files[VersionType.UPDATE][f"{img_hash}.{filename}"] = update_version
            if delta_update:
                filename_w_hash = f"{img_hash_device}.{filename}"
                update_files[VersionType.CURRENT][filename_w_hash] = device_version

        return update_files

    def download_files(self, device_id: str, tag: str,
                       update_info: UpdateInfo, download_dir: str = "download",
                       *, delta_update: bool = False) -> None:
        download_path = Path(download_dir)
        download_path.mkdir(exist_ok=True, parents=True)

        update_files = self._select_update_files(
            device_id, tag, update_info, delta_update=delta_update)

        downloaded_files = {VersionType.UPDATE: {}, VersionType.CURRENT: {}}
        for type_ in [VersionType.UPDATE, VersionType.CURRENT]:
            files = update_files[type_]
            for filename_w_hash in files:
                data = self._api_server_in.get_file(filename_w_hash)
                if data is None:
                    continue

                path = download_path / str(type_)
                path.mkdir(exist_ok=True, parents=True)

                filename = filename_w_hash.split(".", maxsplit=1)[1]
                path = path / filename
                with path.open(mode="wb") as f:
                    f.write(data)

                downloaded_files[type_][filename] = path

        return downloaded_files

    def make_swu_image(self, download_files: dict, keys:Keys, os_version: str,
                       swu_path: str = "update.swu", **cmd_options: any) -> Path | None:

        if not all([VersionType.UPDATE in download_files,
                    VersionType.CURRENT in download_files]):
            return None

        update_files = download_files[VersionType.UPDATE]
        current_files = download_files[VersionType.CURRENT]

        base_dir = None

        uki_path = update_files.get(self.uki_filename)
        if uki_path is not None:
            base_dir = uki_path.parents[0]

        rootfs_path = update_files.get(self.rootfs_filename)
        if rootfs_path is not None:
            base_dir = rootfs_path.parents[0]

        rootfs_path_current = current_files.get(self.rootfs_filename)

        base_dir = base_dir if base_dir is not None else Path.cwd()

        create(uki=uki_path,
               rootfs=rootfs_path,
               rootfs_previous=rootfs_path_current,
               os_version=os_version,
               update_cpio=(base_dir / swu_path),
               sw_description=(base_dir/ "sw-description"),
               swupdate_key_path=Path(keys.sym_key),
               swu_cert=Path(keys.cert_pem),
               swu_key=Path(keys.key_pem),
               **cmd_options,
               )

        return base_dir / swu_path

    def upload_swu(self, swu_file: str, update_info: UpdateInfo) -> list:
        r = self._api_server_ex.post_file(
            str(swu_file), {"version": update_info.os_version, "tag": "swu"})
        # rのステータスコード確認して、
        info_list = []
        if r is not None:
            info = self._api_rstuf.post_targets(r)
            info_list.append(info)

        return info_list

    def delete_swu(self, swu_file:str) -> None:
        r = self._api_rstuf.delete_targets([swu_file])
        if r is not None:
            versions = self._api_server_ex.get_versions(swu_file)
            hashes = versions.get("hashes")
            if hashes is not None:
                for h in hashes.values():
                    self._api_server_ex.delete_file(f"{h}.{swu_file}")

    def create_job(self, device_id: str, workflow: str = "simple.status") -> str | None:
        response = self._api_wfx_north.post_jobs(device_id, workflow)
        if response is None:
            return None
        return response.job_id

    def check_status(self, job_id: str, time_to_wait: int = 60) -> bool:
        for _ in range(time_to_wait):
            response = self._api_wfx_south.get_jobs_using_id(job_id)
            if response.status.state == State.SUCCESS:
                return True
            if response.status.state == State.FAILED:
                break
            time.sleep(1)
        return False

    def update_device_info(self, device_id: str, update_info: UpdateInfo) -> None:
        return self._api_dm.put_device(
            device_id,
            update_info.rootfs_version,
            update_info.uki_version,
            update_info.os_version)

    def add_device(self, device_id: str, update_info: UpdateInfo) -> None:
        return self._api_dm.add_device(
            device_id,
            update_info.rootfs_version,
            update_info.uki_version,
            update_info.os_version,
            update_info.group)

    def delete_device(self, device_id: str) -> None:
        return self._api_dm.delete_device(device_id)

    def get_jobs(self, **kwargs: dict) -> str:
        return self._api_wfx_south.get_jobs(**kwargs)

    def get_devices(self) -> None:
        return self._api_dm.get_devices()
