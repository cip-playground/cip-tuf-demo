# 🤖 Bot for the Update

The bot accesses each server via API, creating SWU files based on the target device's information.

Currently, it supports only the creation of update images and preparation for distribution.

## 🏃‍♂️ create_swu command

The creation of SWU files is available as a CLI tool.
Setup and usage instructions are provided below.

```bash
poetry install
create_swu --help
# Prints:
# ...
# Usage: create_swu [OPTIONS]
# ...

create_swu --uki path_to_uki --rootfs path_to_rootfs --os-version 2.0.0
```
