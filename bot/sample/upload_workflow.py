from pathlib import Path

import yaml

from bot.api import NorthboundAPI

path = Path(__file__).resolve().parents[2] / "wfx/simple.status.yml"

with path.open(mode="r") as file:
    data = yaml.safe_load(file)

api = NorthboundAPI("http://localhost:8091/api/wfx/v1/")
api.post_workflow(data)
