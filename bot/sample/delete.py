import logging

from bot import APIUrls, Bot

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

api_urls = APIUrls(
    device_management="http://localhost:8123/api/v1/",
    server_internal="http://localhost:8010/api/v1/",
    server_external="http://localhost:8011/api/v1/",
    rstuf="http://localhost/api/v1/",
    wfx_north="http://localhost:8091/api/wfx/v1/",
    wfx_south="http://localhost:8090/api/wfx/v1/",
)

bot = Bot(api_urls)
bot.delete_swu("group1.swu")
