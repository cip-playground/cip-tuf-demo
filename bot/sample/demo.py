# TENTATIVE

import logging
import os
import sys
from pathlib import Path

from celery.app import Celery

from bot import APIUrls, Bot, Keys, UpdateInfo

path = Path(__file__).resolve().parents[2]

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

url = os.getenv("RABBITMQ_SERVER_IP")
if url is None:
    logger.error("Please provide the ip-address of the RabbitMQ server")
    sys.exit(1)

broker_url = f"amqp://guest:guest@{url}:5672"

app = Celery("abot", broker=broker_url)
app.conf.result_backend = "rpc://"
app.conf.result_persistent = True

api_urls = APIUrls(
    device_management="http://localhost:8123/api/v1/",
    server_internal="http://localhost:8010/api/v1/",
    server_external="http://localhost:8011/api/v1/",
    rstuf="http://localhost/api/v1/",
    wfx_south="http://localhost:8090/api/wfx/v1/",
    wfx_north="http://localhost:8091/api/wfx/v1/"
)
bot = Bot(api_urls, rootfs_filename="rootfs.img.gz", uki_filename="linux.efi")

key_dir = path / "isar-cip-core/recipes-devtools/swupdate-certificates/files/bookworm"
keys = Keys(
    sym_key = str(""),
    cert_pem = str(key_dir / "PkKek-1-snakeoil.pem"),
    key_pem = str(key_dir / "PkKek-1-snakeoil.key"),
)


update_info = UpdateInfo(
    os_version="2.0.0",
    rootfs_version="2.0.0",
    uki_version="2.0.0",
)

target_device_id = "exampleDeviceId"

@app.task
def update() -> str:
    logger.info("Starting bot")
    logger.info(Path(__file__).resolve().parent / "download")
    downloaded_files = bot.download_files(
        target_device_id, "group1", update_info,
        download_dir= Path(__file__).resolve().parent / "download")
    logger.info(downloaded_files)
    swu_file = bot.make_swu_image(
        download_files=downloaded_files,
        keys=keys,
        os_version=update_info.os_version,
        swu_path="group1.swu",
	uuid_0="fedcba98-7654-3210-cafe-5e0710000001",
	uuid_1="fedcba98-7654-3210-cafe-5e0710000002",
	uki_filename="linux.efi",
        compressed_rootfs="zlib",
        hw_compatibility="cip-core-1.0",
	encrypt=False )
    if swu_file is not None:
        bot.upload_swu(swu_file, update_info)
    logger.info("Done!")
    return True, str(swu_file)

@app.task
def update_with_wfx(*, rdiff: bool = False) -> str:
    logger.info("Starting bot")
    logger.info(Path(__file__).resolve().parent / "download")
    downloaded_files = bot.download_files(
        target_device_id, "group1", update_info,
        download_dir= Path(__file__).resolve().parent / "download", delta_update=rdiff)
    logger.info(downloaded_files)
    if len(downloaded_files) == 0:
        logger.info(bot._get_device_info(target_device_id, "group1"))
        logger.info("No updates available")
        return False, None

    swu_file = bot.make_swu_image(
        download_files=downloaded_files,
        keys=keys,
        os_version=update_info.os_version,
        swu_path="group1.swu",
        uuid_0="fedcba98-7654-3210-cafe-5e0710000001",
        uuid_1="fedcba98-7654-3210-cafe-5e0710000002",
        uki_filename="linux.efi",
        compressed_rootfs="zlib",
        hw_compatibility="cip-core-1.0",
        encrypt=False )
    if swu_file is None:
        return False, None
    logger.info(swu_file)

    job_id = bot.create_job(target_device_id)

    logger.info("Uploading SWU file")

    bot.upload_swu(swu_file, update_info)

    logger.info("Checking job status: %s", job_id)
    if job_id is not None:
        results = bot.check_status(job_id, 60*60)

    if results:
        logger.info("Job completed successfully")
        bot.update_device_info(target_device_id, update_info)
    else:
        logger.error("Job failed")

    return results, str(swu_file)

