# cip-tuf-demo: Demonstration of TUF based software update

This project consists of the following software.

* Server
  * `RSTUF`: RSTUF components for TUF metadata management (created with docker-compose.yml)
  * `server`: File server used to store update artifacts and TUF metadata
  * `device-managemnt`: Device management server used to store device information
  * `wfx`: Workflow executor and jobs for it
* Client (device)
  * `isar-cip-core`: CIP Core image generator (submodule)
  * `isar-cip-core-tuf`: isar-cip-core extention layer so that swupdate suricatta communicates with the TUF server
  * `tuf-client`: Go based client to communicate with RSTUF

# 1. Build demo system

## Clone the repository
```bash
host$ git clone --recurse-submodules  https://gitlab.com/cip-playground/cip-tuf-demo.git
host$ cd cip-tuf-demo
```

Make sure the following packages are installed in the host system
```bash
sudo apt install git build-essential libpq-dev python3 python3-venv python3-dev httpie librsync-dev
```
Additionally, docker and docker-compose must be installed

Go version 1.22 is required to be installed in the host to build `wfxctl` to communicate with the wfx server.

**NOTE**: Remove any older go installations before proceeding with the below commands
```bash
host$ wget https://go.dev/dl/go1.22.0.linux-amd64.tar.gz
host$ sudo rm -rf /usr/local/go && tar -C /usr/local -xzf go1.22.0.linux-amd64.tar.gz
host$ export PATH=$PATH:/usr/local/go/bin
host$ go version
host$ # go version go1.22.0 linux/amd64
```

## Set server IP address

Example:

```bash
host$ ./scripts/1/set-server-ip.sh <HOST-MACHINE-IP>
```

**TODO: Make it possible to dynamically define all the IP address values above**

## File Server Setup

To set up the file server, follow these steps:
```bash
# 1. Navigate to the server directory:
host$ cd server

# 2. Build the Docker image for the file server with the tag latest:
host$ docker build -t s3rver:latest .
host$ cd ../
```

## Device Management Server Setup

To set up the device management server, follow these steps:
```bash
# 1. Navigate to the server directory:
host$ cd device_management

# 2. Build the Docker image for the device management server with the tag latest:
host$ docker build -t dm:latest .
host cd ../
```

## Build CIP Core Images
**NOTE**: Please refer to the relevant [document](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/next/doc/README.secureboot.md?ref_type=heads#secure-boot-on-generic-uefi-x86) related to flashing secure boot keys on a generic x86 device and information specific to [MCOM](https://gitlab.com/cip-project/cip-core/isar-cip-core/-/blob/next/doc/boards/README.m-com-x86.md)

```bash
host$ cp -r suricatta/swupdate_suricatta.lua isar-cip-core-tuf/recipes-core/swupdate-config/files
host$ cp -r tuf-client/* isar-cip-core-tuf/recipes-core/tuf-client/files/tuf-client
host$ cp -r sample/metadata/ isar-cip-core-tuf/recipes-core/tuf-client/files/tuf-client
host$ cp -r heartbeat/* isar-cip-core-tuf/recipes-core/heartbeat/files/heartbeat
host$ cp -r isar-cip-core-tuf isar-cip-core
host$ cd isar-cip-core
```

### QEMU
```
host$ KAS_BUILD_DIR=build ./kas-container build kas-cip.yml:kas/board/qemu-amd64.yml:kas/opt/6.1.yml:kas/opt/bookworm.yml:kas/opt/ebg-swu.yml:isar-cip-core-tuf/kas/tuf-demo.yml:isar-cip-core-tuf/kas/swu-1.0.0.yml
host$ KAS_BUILD_DIR=build-qemu-2.0.0 ./kas-container build kas-cip.yml:kas/board/qemu-amd64.yml:kas/opt/6.1.yml:kas/opt/bookworm.yml:kas/opt/ebg-swu.yml:isar-cip-core-tuf/kas/tuf-demo.yml:isar-cip-core-tuf/kas/swu-2.0.0.yml

host$ cd ../
```

### Generic x86 device (The following commands builds the security image for a generic x86 device)
**Note**: please disable the watchdog by adding `WDOG_TIMEOUT = "0"` to `isar-cip-core/conf/machine/x86-uefi.conf` file for M-COM 
```
host$ KAS_BUILD_DIR=build-x86-1.0.0 ./kas-container build kas-cip.yml:kas/board/x86-uefi.yml:kas/opt/6.1.yml:kas/opt/bookworm.yml:kas/opt/security.yml:isar-cip-core-tuf/kas/tuf-demo.yml:isar-cip-core-tuf/kas/swu-1.0.0.yml
host$ KAS_BUILD_DIR=build-x86-2.0.0 ./kas-container build kas-cip.yml:kas/board/x86-uefi.yml:kas/opt/6.1.yml:kas/opt/bookworm.yml:kas/opt/security.yml:isar-cip-core-tuf/kas/tuf-demo.yml:isar-cip-core-tuf/kas/swu-2.0.0.yml

host$ cd ../
```

# 2. Setup demo system

## Docker Compose Deployment

The commands below illustrate how to use Docker Compose for both external and internal services:
```bash
# Starting External Services:
host$ docker compose -p external up -d

# Starting Internal Services:
host$ docker compose -f docker-compose-internal.yml -p internal up -d

# Shutting Down Services:
# To exit and remove volumes, use the following commands:
host$ docker compose -p external down -v
host$ docker compose -p internal down -v
```
## Install RSTUF CLI

```bash
host$ python3 -m venv venv && . venv/bin/activate
(venv) $ pip install --no-cache-dir repository-service-tuf[sqlalchemy,psycopg2]==0.12.0b1
(venv) $ python -c "import pkg_resources; print(pkg_resources.get_distribution('repository-service-tuf').version)"
# Prints:
# 0.12.0b1
```

## Set up the initial configuration for the RSTUF server.

```bash
(venv) $ rstuf admin-legacy ceremony -b -u --api-server http://localhost -f sample/payload.json
# Prints:
# Starting online bootstrap
# Bootstrap status: ACCEPTED (4716248b9e7b4bdbb6e39fd79316df1d)
# Bootstrap status:  STARTED
# .Bootstrap status:  SUCCESS
# Bootstrap completed using `sample/payload.json`.

# exit from python virtual environment
(venv) $ deactivate
```

Confirm whether the configuration is set correctly, verify that the "state" is "finished."
```bash
host$ http GET http://localhost/api/v1/bootstrap/ Accept:application/json
# Prints:
# HTTP/1.1 200 OK
# content-length: 127
# content-type: application/json
# date: Fri, 08 Mar 2024 16:11:27 GMT
# server: uvicorn
#
# {
#     "data": {
#         "bootstrap": true,
#         "id": "31fb49290f4448f1970d68d68e776cd2",
#         "state": "finished"
#     },
#     "message": "System LOCKED for bootstrap."
# }
```

Confirm that there is access to the metadata
```bash
host$ http http://localhost:8011/api/v1/metadata/1.root.json --head
# Prints:
# HTTP/1.1 200 OK
# content-length: 2050
# content-type: application/json
# date: Fri, 08 Mar 2024 16:12:34 GMT
# etag: "f61c4d829bf73f99591127538b1ad6d2"
# last-modified: Fri, 08 Mar 2024 16:11:20 GMT
# server: uvicorn
```
**NOTE**: The rstuf admin-legacy command is deprecated. Future versions will use rstuf admin.

##  wfx Setup

wfx is used as a Docker container. Here, build the wfx-cli tool (wfxctl), which is a tool that interacts with the wfx API, and register the workflow.
First, let's build the tool.
```bash
# 1. Navigate to wfx directory:
host$ cd wfx/wfx

# 2. Build the cli-tool for wfx:
host$ make wfxctl
```
To register a workflow for managing simple statuses, use the simple.status.yml file.
```bash
# in wfx/wfx

# 1. Creating a workflow using simple.status.yml
host$ ./wfxctl workflow create --filter=.transitions ../simple.status.yml --mgmt-port 8091
# Prints:
# Aug  2 12:57:19 INF Creating workflows count=1
# Aug  2 12:57:19 INF Created new workflow name=simple.status
# ...

# 2. Confirming if the workflow is registered
host$ ./wfxctl workflow query --client-port 8090  | jq -r '.content[0].description'
# Prints:
# A workflow to track the progress of software updates
host$ cd ../../
```
## Move Images

Move the created images to the artifact directories.
```bash
# For QEMU
host$ mkdir -p artifacts/1.0.0
host$ ARTIFACT_DIR=isar-cip-core/build/tmp/deploy/images/qemu-amd64
host$ cp "${ARTIFACT_DIR}/cip-core-image-cip-core-bookworm-qemu-amd64.squashfs.gz" artifacts/1.0.0/rootfs.img.gz
host$ cp "${ARTIFACT_DIR}/linux.efi" artifacts/1.0.0/

host$ mkdir -p artifacts/2.0.0
host$ ARTIFACT_DIR=isar-cip-core/build-qemu-2.0.0/tmp/deploy/images/qemu-amd64
host$ cp "${ARTIFACT_DIR}/cip-core-image-cip-core-bookworm-qemu-amd64.squashfs.gz" artifacts/2.0.0/rootfs.img.gz
host$ cp "${ARTIFACT_DIR}/linux.efi" artifacts/2.0.0/

# For M-COM
host$ mkdir -p artifacts/1.0.0
host$ ARTIFACT_DIR=isar-cip-core/build-x86-1.0.0/tmp/deploy/images/x86-uefi
host$ cp "${ARTIFACT_DIR}/cip-core-image-security-cip-core-bookworm-x86-uefi.verity.gz" artifacts/1.0.0/rootfs.img.gz
host$ cp "${ARTIFACT_DIR}/linux.efi" artifacts/1.0.0/

host$ mkdir -p artifacts/2.0.0
host$ ARTIFACT_DIR=isar-cip-core/build-x86-2.0.0/tmp/deploy/images/x86-uefi
host$ cp "${ARTIFACT_DIR}/cip-core-image-security-cip-core-bookworm-x86-uefi.verity.gz" artifacts/2.0.0/rootfs.img.gz
host$ cp "${ARTIFACT_DIR}/linux.efi" artifacts/2.0.0/
```

## Device Management

To handle the device version information, the device is registered as follows:
```bash
http -f POST http://localhost:8123/api/v1/devices/ \
  device_id=exampleDeviceId \
  rootfs_version=1.0.0 \
  uki_version=1.0.0 \
  os_version=1.0.0 \
  tag=group1

# Prints:
# HTTP/1.1 200 OK
# content-length: 29
# content-type: application/json
# date: Fri, 12 Jul 2024 15:13:25 GMT
# server: uvicorn

# {
#     "targets": "exampleDeviceId"
# }
```
If registered correctly, a status code of 200 can be confirmed.

In addition to the device ID and version information, the tag “group1” is added.

The purpose of the tag is to control the distribution range by specifying the DeviceID or tag during delivery.
The device information can be as follows:
```bash
host$ http http://localhost:8123/api/v1/devices/exampleDeviceId --body
# Prints:
# {
#     "os_version": "1.0.0",
#     "rootfs_version": "1.0.0",
#     "tag": "group1",
#     "uki_version": "1.0.0"
# }
```
## Image Registration
The images generated during setup are registered to the Internal file server. Repeat the below commands for `VERSION="1.0.0"` and `VERSION="2.0.0"`
```bash
host$ VERSION="1.0.0" # and 2.0.0
host$ http -f POST http://localhost:8010/api/v1/artifacts \
    file@artifacts/${VERSION}/rootfs.img.gz version=${VERSION} tag=group1
	
host$ http -f POST http://localhost:8010/api/v1/artifacts \
    file@artifacts/${VERSION}/linux.efi version=${VERSION} tag=group1
```
Status code of 200 when POSTing indicates the upload was successful.

The list of hash values of the uploaded files can be obtained as follows:
```bash
host$ http GET "http://localhost:8010/api/v1/artifacts/rootfs.img.gz/hashes" | jq ".hashes"
# Prints:
# {
#   "1.0.0": "4110277a55857228b6da273dbdde723616eeb26ac868fa3ac35a84643b690afb",
#   "2.0.0": "88474f829a68bb64f09cb7116d0e21af40b6c6ef68c841ee413c9dd3d7687058"
# }
```

## UI Setup

Here is a simple GUI provided in the demo directory. First, follow the below steps to set up the environment.
```bash
host$ cd demo
host$ python3 -m venv venv && . venv/bin/activate
(venv) pip install -r requirements.txt
```
Required settings can be specified in config.toml. The API URL and other configurations can be modified.
The path to the keys is required. They can be registered as follows:
```bash
[keys]
sym_key = "path_to_sym_key"
cert_pem = "path_to_*.cert.pem"
key_pem = "path_to_*.key.pem"
```
Here are the steps to start the server:
```
(venv) export RABBITMQ_SERVER_IP="<ip-address>"
(venv) celery -A task.app worker --loglevel=INFO & python demo.py & python mqtt.py
```
After starting, The UI can be accessed at localhost:5000.

## Calling the task with GUI

When accessing localhost:5000, there is a section called "Device Information" on the home screen. Click on `exampleDeviceId` to view the device information.
On the page, there is a section called "Update Device" with input forms for OS, UKI and ROOTFS versions. Fill in all of them with "2.0.0" and click the "Start Task" button to initiate the update. Enable delta update by clicking checkbox (rdiff).

The page will update, and the "Update Device" section will display the ongoing wfx tasks.

## Calling the task without GUI

By executing `update_with_wfx.delay(rdiff=True)`, an asynchronous job is created, which allows to check the status while it's in progress. By specifying `rdiff=True`, a delta update image will be generated and used. If a regular update image is required, please specify `False`. (If wfx is not required, please use `update.delay()`.)
```bash
cd demo
. venv/bin/activate
export RABBITMQ_SERVER_IP="<ip-address>"
python
>>> from task import update_with_wfx
>>> t = update_with_wfx.delay("exampleDeviceId", "2.0.0", "2.0.0", "2.0.0", rdiff=True)
>>> t.ready()
False
```

The bot creates an update image  and uploads the image to the external file server. After that, it creates a job with wfx and uploads the artifact information to RSTUF. This initiates the update process on the device. `t.ready()` will return `False` until the update on the device is complete (success/failed).

wfxctl can also be used to directly access wfx and check the status of jobs.

```bash
host$ ./wfx/wfx/wfxctl job query --client-id exampleDeviceId --client-port 8090 | jq -r '.content[0].id' | xargs -I {} ./wfx/wfx/wfxctl job get --id {} --client-port 8090 | jq -r '.status.state'
# Prints:
# PROGRESS
```

# 3. Run demo system

**NOTE:** The steps to run the demo on a generic x86 device are similar to the steps in QEMU.
## Boot device (QEMU)

```bash
host$ cd isar-cip-core
host$ DISTRO_RELEASE=bookworm SWUPDATE_BOOT=true ./start-qemu.sh x86
```

## Run swupdate
In QEMU, the running swupdate automatically detects the update and starts the update. If the words "SWUPDATE successful" are seen in the logs, the update is complete. In addition, if wfx is being used, wfxctl can be used to confirm that the status has transitioned to "DONE".
```bash
qemu$ swupdate -l 1 --suricatta '-S lua --version-path /etc/sw-versions --version-pattern software="([^"]+)" --group-id group1 --progress-url http://<ip-address>:8090/api/wfx/v1 --broker tcp://<ip-address>:1883' -v
# in QEMU
# ...
# [INFO ] : SWUPDATE running :  [notify_helper] : [StateManager:updateJobStatus] Job status updated to PROGRESS
# ...
# [DEBUG] : SWUPDATE running :  [notify_helper] : [run_command] Command success with exit code 0: ./tufclient post group1.swu --localTargetsDir /tmp/ --targetsURL http://<ip-address>:8011/api/v1/artifacts --metadataURL http://<ip-address>:8011/api/v1/metadata
# ...
# [INFO ] : SWUPDATE started :  Software Update started !
# ...
# [INFO ] : SWUPDATE running :  [notify_helper] : [has_pending_action] Prepare success for target file: 03cf504264402058219169fd321fd6c1bc1efbc2336c00620e617f9b424bdfb6.group1.swu
# ...
# [INFO ] : SWUPDATE successful ! SWUPDATE successful !
# ...
# [INFO ] : SWUPDATE running :  [notify_helper] : [StateManager:updateJobStatus] Job status updated to DONE
```
Once confirmed that the update is complete, restart the device.

**NOTE**: Exit the swupdate process by pressing `Ctrl + C` before running the reboot command
```bash
qemu$ reboot

# After reboot
qemu$ cat /etc/sw-versions
# Prints:
# software "2.0.0"
```

Currently, since there is no test after reboot, it is necessary to manually update the status. Update the ustate and the status of the wfx job to "success" as follows:
```bash
(qemu) $ bg_setenv -c
# Prints:
# Environment update was successful.

(qemu) $ bg_printenv -c -o revision,ustate
# Prints:
# Using latest config partition
# Values:
# revision:         2
# ustate:           0 (OK)

(qemu) $ swupdate -l 3 --suricatta '-S lua --progress-url http://<ip-address>:8090/api/wfx/v1 --update-state SUCCESS'
# Prints:
# ...
# [INFO ] : SWUPDATE running :  [notify_helper] : [StateManager:updateJobStatus] Job status updated to SUCCESS
```

By updating wfx, the currently running task can be completed and check the results.
```bash
# in bot/sample & in Python
>>> t.ready()
True
>>> t.result
[True, '<working directory>/cip-tuf-demo/bot/sample/download/group1.swu']
```

The status of the job can you also be checked on the wfx server.
```bash
host$ ./wfx/wfx/wfxctl job query --client-id exampleDeviceId --client-port 8090 | jq -r '.content[0].id' | xargs -I {} ./wfx/wfx/wfxctl job get --id {} --client-port 8090 | jq -r '.status.state'
# Prints:
SUCCESS
```

You can also verify that the device information has been updated properly.
```bash
host$ http http://localhost:8123/api/v1/devices/exampleDeviceId --body
# Prints:
# {
#     "os_version": "2.0.0",
#     "rootfs_version": "2.0.0",
#     "tag": "group1",
#     "uki_version": "2.0.0"
# }
```
# License

cip-tuf-demo consists of multiple open source projects.

The following components are clone of or based on existing projects and
are licensed under the same license as the base projects.

* `isar-cip-core` (submodule) : MIT
* `wfx` (submodule) : Apache-2.0
* `RSTUF`: MIT
* `app` : MIT
  * This sample application is based on the Next.js template

Other components are licensed under the Apache License, Version 2.0

* `isar-cip-core-tuf/recipes-core/tuf-client`
* `server`
* `tuf-client`
* Others like helper scripts
