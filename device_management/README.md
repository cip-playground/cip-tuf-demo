# 🖥️ Device Management

## 🏃‍♂️ run

### 🐳 docker

Use Docker to build and run the server:

```bash
# Build
docker build -t dm:latest .

# Check
docker run --rm dm:latest --help
# Prints:
#  Usage: dm [OPTIONS]
#  ...

# Run
docker run -d --rm --name temp_container \
    -p 8123:8123 \
    dm:latest
```

## 🌐 API

After starting the server using Docker, you can access the FastAPI documentation and try out the API endpoints by navigating to [http://localhost:8123/docs/](http://localhost:8123/docs/).
This page provides an interactive API documentation interface where you can send requests to your API and see the responses.

```bash
http -f POST http://localhost:8123/api/v1/devices/ \
  device_id=exampleDeviceId rootfs_version=1.0.0 uki_version=1.0.0 tag=group1

http -f GET "http://localhost:8123/api/v1/devices/exampleDeviceId" --body
# Prints:
# {
#     "os_version": "1.0.0",
#     "rootfs_version": "1.0.0",
#     "tag": "group1",
#     "uki_version": "1.0.0"
# }

http -f PUT http://localhost:8123/api/v1/devices/ \
    device_id=exampleDeviceId uki_version=2.0.0 os_version=2.0.0

http -f GET "http://localhost:8123/api/v1/devices/exampleDeviceId" --body
# Prints
# {
#     "os_version": "2.0.0",
#     "rootfs_version": "1.0.0",
#     "tag": "group1",
#     "uki_version": "2.0.0"
# }
```

## 👨‍💻 Dev

Run the following command to perform a development check on the server:

```bash
ruff check device_management
```
