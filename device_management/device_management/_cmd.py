import os
import sys

import typer
import uvicorn

from .main import create_app

app = typer.Typer()


@app.command("run-server")
def cmd_run_server(
    port: int = 8123,
    host: str = "127.0.0.1",
    database_url: str = "sqlite:///./test.db",
) -> None:

    app_ = create_app(db_url=os.environ.get("DATABASE_URL", database_url))

    try:
        uvicorn.run(app_, host=host, port=port)
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == "__main__":
    app()
