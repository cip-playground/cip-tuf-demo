from datetime import datetime
from enum import Enum
from pathlib import Path
from zoneinfo import ZoneInfo

import semver
from fastapi import APIRouter, FastAPI, Form, HTTPException, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from sqlalchemy import Column, DateTime, Integer, String, and_, create_engine, func
from sqlalchemy.engine import Engine
from sqlalchemy.exc import DatabaseError, IntegrityError
from sqlalchemy.orm import declarative_base, sessionmaker


class DeviceStatus(Enum):
    NORMAL = 0
    ERROR = 1
    INITIALIZING = 9

Base = declarative_base()

class Device(Base):
    __tablename__ = "targets"

    device_id = Column(String, primary_key=True, index=True)
    tag = Column(String, nullable=True)
    os_version = Column(String, nullable=True)
    rootfs_version = Column(String, nullable=False)
    uki_version = Column(String, nullable=False)
    device_status = Column(Integer, nullable=False,
                           default=DeviceStatus.INITIALIZING.value)
    last_updated = Column(DateTime, server_default=func.now(), onupdate=func.now())

    def to_dict(self) -> dict:
        return {self.device_id: {
            "tag": self.tag, "rootfs_version": self.rootfs_version,
            "uki_version": self.uki_version, "os_version": self.os_version,
            "status": self.device_status,
            "last_updated": self.last_updated.isoformat()}}

class DeviceModel(BaseModel):
    device_id: int
    tag: str
    rootfs_version: str
    uki_version: str
    os_version: str
    device_status: int
    last_updated: str

def wrapper_create_engine(url: str) -> Engine:
    db_name = url.split(":")[0]
    connect_args = {"check_same_thread": False} if db_name == "sqlite" else {}
    return create_engine(url, connect_args=connect_args)

def create_app(  # noqa: C901 PLR0915
    db_url: str | Path,
    ) -> FastAPI:

    engine = wrapper_create_engine(db_url)
    Base.metadata.create_all(bind=engine)

    session_local = sessionmaker(autocommit=False, autoflush=False, bind=engine)

    db = session_local()

    app = FastAPI(title="Device Management API")

    router_v1 = APIRouter()

    @router_v1.get("/devices/")
    async def get_devices() -> JSONResponse:
        devices_dict = {}
        try:
            db_devices = db.query(Device).all()
            if db_devices is not None:
                for device in db_devices:
                    devices_dict.update(device.to_dict())
        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e

        return JSONResponse(status_code=status.HTTP_200_OK, content=devices_dict)

    @router_v1.post("/devices/")
    def create_device(device_id: str=Form(...),
                    rootfs_version: str=Form(...),
                    uki_version: str=Form(...),
                    tag: str | None=Form(None),
                    os_version: str | None=Form(None),
                    ) -> JSONResponse:
        try:
            _ = semver.Version.parse(rootfs_version)
            _ = semver.Version.parse(uki_version)
            if os_version is not None:
                _ = semver.Version.parse(os_version)

            _db_device = Device(
                device_id=device_id,
                rootfs_version=rootfs_version,
                uki_version=uki_version,
                os_version=os_version,
                tag=tag,
                )
            db.add(_db_device)
            db.commit()
        except TypeError as e0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST) from e0
        except IntegrityError as e1:
            db.rollback()
            raise HTTPException(status_code=status.HTTP_409_CONFLICT) from e1
        except (OSError, MemoryError) as e2:
            db.rollback()
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                detail="Something happened") from e2

        return JSONResponse(
                    status_code=status.HTTP_200_OK,
                    content={"targets": device_id})

    @router_v1.put("/devices/")
    def update_device(device_id: str=Form(...),  # noqa: C901 PLR0912 PLR0913
                    tag: str | None=Form(None),
                    rootfs_version: str | None=Form(None),
                    uki_version:  str | None=Form(None),
                    os_version:  str | None=Form(None),
                    device_status: int | None=Form(None),
                    ) -> JSONResponse:

        if ((rootfs_version is None) and (uki_version is None) and
            (tag is None) and (os_version is None) and (device_status is None)):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)

        try:
            db_device = db.query(Device).filter(
                and_(Device.device_id == device_id),
                ).first()
            if db_device is None:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

            if rootfs_version is not None:
                result = semver.compare(rootfs_version, db_device.rootfs_version)
                if result == 1:
                    db_device.rootfs_version = rootfs_version
                elif result <= 0:
                    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)

            if uki_version is not None:
                result = semver.compare(uki_version, db_device.uki_version)
                if result == 1:
                    db_device.uki_version = uki_version
                elif result <= 0:
                    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)

            if os_version is not None:
                if db_device.os_version is None:
                    try:
                        _ = semver.Version.parse(os_version)
                        db_device.os_version = os_version
                    except TypeError as e:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST) from e
                else:
                    result = semver.compare(os_version, db_device.os_version)
                    if result == 1:
                        db_device.os_version = os_version
                    elif result <= 0:
                        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)

            if tag is not None:
                db_device.tag = tag

            if device_status is not None:
                db_device.device_status = device_status
                db_device.last_updated = datetime.now(ZoneInfo("Asia/Tokyo"))

            db.commit()

        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e

        return JSONResponse(
                    status_code=status.HTTP_200_OK,
                    content={"targets": device_id})

    @router_v1.delete(
        "/devices/{device_id}")
    async def delete_device(device_id: str) -> JSONResponse:

        try:
            db_device = db.query(Device).filter(
                and_(Device.device_id == device_id),
                ).first()
            if db_device is not None:
                db.delete(db_device)
            else:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e
        db.commit()

        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content={"detail": f"{device_id} is deleted."})

    @router_v1.get(
        "/devices/{device_id}")
    async def get_device(device_id: str) -> JSONResponse:

        try:
            db_device = db.query(Device).filter(
                and_(Device.device_id == device_id),
                ).first()
        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e

        if db_device is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

        content = {"rootfs_version": db_device.rootfs_version,
                    "uki_version": db_device.uki_version,
                    "status": db_device.device_status,
                    "last_updated": db_device.last_updated.isoformat()}

        if (tag:=db_device.tag) is not None:
            content["tag"]= tag

        if (os_version:=db_device.os_version) is not None:
            content["os_version"]= os_version

        return JSONResponse(
            status_code=status.HTTP_200_OK,
            content=content)

    @router_v1.get(
        "/devices/tag/{tag}")
    async def get_device_by_tag(tag: str) -> JSONResponse:
        devices_dict = {}
        try:
            db_devices = db.query(Device).filter(
                and_(Device.tag == tag),
                ).all()
            if db_devices is not None:
                for device in db_devices:
                    devices_dict.update(device.to_dict())
        except DatabaseError as e:
            raise HTTPException(status_code=500, detail="Database error") from e

        return JSONResponse(status_code=status.HTTP_200_OK, content=devices_dict)

    app.include_router(router_v1, prefix="/api/v1")

    return app
