# README.md

## 👨‍💻 settings

The installation and preparation will be done as follows.

```bash
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

You can specify the settings in `config.toml`.
You can modify the API URL and other configurations.

The path to the keys is required. You need to register them as follows:

```toml
[keys]
sym_key = "path_to_sym_key"
cert_pem = "path_to_*.cert.pem"
key_pem = "path_to_*.key.pem"
```

## 🏃‍♂️ run

Start Celery as shown below.

```bash
export RABBITMQ_SERVER_IP="<ip-address>"
celery -A task.app worker --loglevel=info
# celery -A task.app flower
```

To start the application and check its operation at `http://127.0.0.1:5000`, use the following command:

```bash
export RABBITMQ_SERVER_IP="<ip-address>"
python demo.py
```
