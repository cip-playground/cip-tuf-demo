import redis
from celery.local import PromiseProxy
from flask import Flask, Response, jsonify, render_template, request
from flask_assets import Bundle, Environment

from bot import Bot, UpdateInfo


def compile_assets(app: Flask) -> None:
    assets = Environment(app)
    assets.url = app.static_url_path
    scss = Bundle(
        "main.scss", filters="libsass", output="main.css", depends="scss/*.scss")
    assets.register("scss_all", scss)
    scss.build()

def create_app(bot: Bot , task_func: PromiseProxy) -> Flask:  # noqa: C901 PLR0915
    app = Flask(__name__)

    compile_assets(app)

    redis_client = redis.StrictRedis(host="localhost", port=6379, db=0)

    @app.route("/", methods=["GET"])
    def home() -> Response:
        devices = bot.get_devices()
        if devices is None:
            devices = {}

        files_in = {}
        for filename in [bot.rootfs_filename, bot.uki_filename]:
            files_in[filename] = bot.get_file_versions(filename).get("hashes", {})

        files_ex = {}
        for filename in {v.get("tag")+".swu" for v in devices.values()}:
            files_ex[filename] = bot.get_file_versions(filename).get("hashes", {})

        return render_template("home.html", devices=devices, files=files_in,
                               files_ex=files_ex)

    @app.route("/devices", methods=["GET"])
    def devices() -> Response:
        devices = bot.get_devices()
        if devices is None:
            devices = {}

        files_in = {}
        for filename in [bot.rootfs_filename, bot.uki_filename]:
            files_in[filename] = bot.get_file_versions(filename).get("hashes", {})

        files_ex = {}
        for filename in {v.get("tag")+".swu" for v in devices.values()}:
            files_ex[filename] = bot.get_file_versions(filename).get("hashes", {})

        return render_template("devices.html", devices=devices, files=files_in,
                               files_ex=files_ex)

    @app.route("/<device_id>", methods=["GET", "DELETE"])
    def device(device_id: str) -> Response:
        if request.method == "GET":
            info = bot.get_device_info(device_id)
            if info is None:
                return "Not Found", 404
            return render_template("device.html", device_id=device_id)

        if request.method == "DELETE":
            r = bot.delete_device(device_id)
            if r is None:
                return "Not Found", 404
            return jsonify({"device_id": device_id}), 200
        return "Method Not Allowed", 405

    @app.route("/start_task", methods=["POST"])
    def start_task() -> Response:
        device_id = request.form.get("device_id")
        os_version = request.form.get("os_version")
        uki_version = request.form.get("uki_version")
        rootfs_version = request.form.get("rootfs_version")
        rdiff = request.form.get("rdiff", "false").lower() == "true"

        existing_task_id = redis_client.get(device_id)
        if existing_task_id:
            existing_task = task_func.AsyncResult(existing_task_id)
            if existing_task.state in ["PENDING", "STARTED"]:
                return jsonify({"error": "Task already running for this device"}), 409

        task = task_func.apply_async(
            kwargs={"target_device_id": device_id,
                    "os_version": os_version,
                    "rootfs_version": rootfs_version,
                    "uki_version": uki_version,
                    "rdiff": rdiff })
        return jsonify({"task_id": task.id}), 202

    @app.route("/device", methods=["POST"])
    def add_device() -> Response:
        device_id = request.form.get("device_id")
        os_version = request.form.get("os_version")
        uki_version = request.form.get("uki_version")
        rootfs_version = request.form.get("rootfs_version")
        group = request.form.get("group")
        if os_version is None or uki_version is None or rootfs_version is None:
            return jsonify({"error": "Missing version"}), 400
        r = bot.add_device(device_id,
                           UpdateInfo(os_version, uki_version, rootfs_version, group))
        if r is None:
            return jsonify({"error": "Failed to add"}), r.status_code
        return jsonify({"device_id": device_id}), 200

    @app.route("/task/<task_id>/status")
    def task_status(task_id: str) -> Response:
        task = task_func.AsyncResult(task_id)
        if task.state == "PENDING":
            response = {"state": task.state, "status": "Pending..."}
        elif task.state != "FAILURE":
            response = {"state": task.state, "status": task.info}
        else:
            response = {"state": task.state, "status": task.info}
        return jsonify(response)

    @app.route("/<device_id>/taskid", methods=["GET"])
    def check_running_task(device_id: str) -> Response:
        existing_task_id = redis_client.get(device_id)
        if existing_task_id:
            return jsonify({"task_id": existing_task_id.decode("utf-8")})
        return jsonify({"task_id": None})

    @app.route("/<device_id>/info", methods=["GET"])
    def get_device_info(device_id: str) -> Response:
        device_info = bot.get_device_info(device_id)
        return jsonify({"rootfs": device_info.rootfs_version,
                        "uki": device_info.uki_version,
                        "os": device_info.os_version,
                        "status": device_info.status,
                        "last_updated": device_info.last_updated})

    @app.route("/<device_id>/job", methods=["GET"])
    def get_device_job_log(device_id: str) -> Response:
        job_list = bot.get_jobs(client_id=device_id, **request.args)
        job_info_list = []
        for content in job_list.content:
            job_info = {
                "job_id": content.job_id,
                "mtime": content.mtime,
                "state": content.status.state,
            }
            job_info_list.append(job_info)
        return jsonify(job_info_list)
    return app
