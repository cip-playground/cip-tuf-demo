import logging
import os
import sys
import tomllib
from pathlib import Path

import redis
from celery import Celery, signals
from celery.local import PromiseProxy

from bot import APIUrls, Bot, Keys, UpdateInfo
from bot.bot import VersionType

path = Path(__file__).resolve().parents[2]

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


if not Path("config.toml").exists():
    config = {}
else:
    with Path("config.toml").open("rb") as f:
        config = tomllib.load(f)

url = os.getenv("RABBITMQ_SERVER_IP",
                config.get("rabbitmq", {}).get("server_ip", "localhost"))
if not url:
    logger.error("Please provide the ip-address of the RabbitMQ server")
    sys.exit(1)

app = Celery("abot", broker=f"amqp://guest:guest@{url}:5672")
app.conf.result_backend = config.get("celery", {}).get("result_backend", "rpc://")
app.conf.result_persistent = config.get("celery", {}).get("result_persistent", True)

config_api_urls = config.get("api_urls", {})
api_urls = APIUrls(
    device_management=config_api_urls.get(
        "device_management", "http://localhost:8123/api/v1/"),
    server_internal=config_api_urls.get(
        "server_internal", "http://localhost:8010/api/v1/"),
    server_external=config_api_urls.get(
        "server_external", "http://localhost:8011/api/v1/"),
    rstuf=config_api_urls.get(
        "rstuf", "http://localhost/api/v1/"),
    wfx_south=config_api_urls.get(
        "wfx_south", "http://localhost:8090/api/wfx/v1/"),
    wfx_north=config_api_urls.get(
        "wfx_north", "http://localhost:8091/api/wfx/v1/"),
)
bot = Bot(api_urls, rootfs_filename="rootfs.img.gz", uki_filename="linux.efi")

config_keys = config.get("keys", {})

cert_pem = config_keys.get("cert_pem")
key_pem = config_keys.get("key_pem")
if (cert_pem is None) or (key_pem is None):
    logger.error("Please provide the path to the certificate and key files")
    sys.exit(1)

keys = Keys(
    sym_key = config_keys.get("sym_key"),
    cert_pem = cert_pem,
    key_pem = key_pem,
)

redis_client = redis.StrictRedis(host="localhost", port=6379, db=0)

@app.task(bind=True)
def update_with_wfx(  # noqa: PLR0913
    self,  # noqa: ANN001
    target_device_id: str,
    os_version: str,
    rootfs_version: str | None = None,
    uki_version: str | None = None,
    *, rdiff: bool = False) -> tuple[bool, str | None]:

    logger.info("Starting bot")
    redis_client.set(target_device_id, self.request.id)

    logger.info("Downloading files for %s", target_device_id)
    update_info = UpdateInfo(
        os_version=os_version,
        rootfs_version=rootfs_version,
        uki_version=uki_version,
        group="group1"
    )

    downloaded_files = bot.download_files(
        target_device_id, "group1", update_info,
        download_dir= Path(__file__).resolve().parent / "download",
        delta_update=rdiff)
    # Fix hardcoded-tag ("group1") noqa: FIX002
    logger.debug(downloaded_files)

    if len(downloaded_files[VersionType.UPDATE]) == 0:
        logger.error("No updates available")
        return False, None

    if rdiff and len(downloaded_files[VersionType.CURRENT]) == 0:
        logger.error("No current files available")
        return False, None

    swu_file = bot.make_swu_image(
        download_files=downloaded_files,
        keys=keys,
        os_version=update_info.os_version,
        swu_path="group1.swu",
        uuid_0="fedcba98-7654-3210-cafe-5e0710000001",
        uuid_1="fedcba98-7654-3210-cafe-5e0710000002",
        uki_filename="linux.efi",
        compressed_rootfs="zlib",
        hw_compatibility="cip-core-1.0",
        encrypt=False)
    if swu_file is None:
        logger.error("Failed to create SWU file")
        return False, None
    logger.debug(swu_file)

    logger.info("Creating job")
    job_id = bot.create_job(target_device_id)
    logger.info(job_id)

    logger.info("Uploading SWU file")
    bot.upload_swu(swu_file, update_info)

    logger.info("Checking job status: %s", job_id)
    if job_id is not None:
        results = bot.check_status(job_id, 60*60)

    if results:
        logger.info("Job completed successfully")
        bot.update_device_info(target_device_id, update_info)
    else:
        logger.error("Job failed")

    return results, str(swu_file)

def cleanup_task_data(device_id: str) -> None:
    redis_client.delete(device_id)

@signals.task_success.connect
def task_success_handler(sender: PromiseProxy | None = None,
                         result: tuple[bool, str | None] | None = None,  # noqa: ARG001
                         **kwargs: dict) -> None:  # noqa: ARG001
    cleanup_task_data(sender.request.kwargs["target_device_id"])

@signals.task_failure.connect
def task_failure_handler(sender: PromiseProxy | None = None,
                         exception: Exception | None = None,  # noqa: ARG001
                         **kwargs: dict) -> None:  # noqa: ARG001
    cleanup_task_data(sender.request.kwargs["target_device_id"])
