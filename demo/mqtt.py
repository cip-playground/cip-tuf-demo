import logging
import re

import paho.mqtt.client as mqtt
import requests

logger = logging.getLogger("MQTTLogger")
logger.setLevel(logging.INFO)

console_handler = logging.StreamHandler()

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class MQTTSubscriber:
    def __init__(self, broker_address: str, port: int=1883) -> None:
        self.client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,
                                  protocol=mqtt.MQTTv5)
        self.broker_address = broker_address
        self.port = port
        self.topic = "device/+/heartbeat"

        self.api_url = "http://localhost:8123/api/v1/devices/"
        self.pattern = re.compile(r"device/([^/]+)/heartbeat")

        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect

    def on_connect(self, client: mqtt.Client, userdata: list, flags: dict,  # noqa: ARG002
                   rc: str, properties: dict|None=None,  # noqa: ARG002
                   ) -> None:
        logger.info("Connected with result code %s", rc)
        self.client.subscribe(self.topic)

    def on_message(self, client: mqtt.Client, userdata: list, msg: str) -> None:  # noqa: ARG002
        if match := self.pattern.search(msg.topic):
            device_id = match.group(1)
        else:
            return
        device_status = msg.payload.decode()
        data = {
            "device_id": device_id,
            "device_status": device_status,
        }
        response = requests.put(self.api_url, data=data, timeout=20)
        if response.status_code == requests.codes.ok:
            logger.info(
                "Successfully updated device %s with status %s",
                device_id, device_status)
        else:
            logger.warning(
                "Failed to update device %s: %d", device_id, response.status_code)
        return

    def on_disconnect(
        self, client: mqtt.Client, userdata: list, rc: int,  # noqa: ARG002
        properties: dict|None=None) -> None:  # noqa: ARG002
        if rc != 0:
            logger.warning("Unexpected disconnection.")

    def connect(self) -> None:
        self.client.connect(self.broker_address, self.port, 60)

    def loop_forever(self) -> None:
        self.client.loop_forever()


if __name__ == "__main__":
    mqtt_subscriber = MQTTSubscriber("localhost")
    mqtt_subscriber.connect()
    try:
        mqtt_subscriber.loop_forever()
    except KeyboardInterrupt:
        logger.info("Program interrupted by user.")
