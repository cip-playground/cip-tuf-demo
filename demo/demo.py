from main import create_app

try:
    from task import bot, update_with_wfx
except ImportError:
    import os
    import tomllib
    from pathlib import Path

    from celery import Celery

    from bot import APIUrls, Bot

    if not Path("config.toml").exists():
        config = {}
    else:
        with Path("config.toml").open("rb") as f:
            config = tomllib.load(f)

    url = os.getenv("RABBITMQ_SERVER_IP")
    c = Celery("abot", broker=f"amqp://guest:guest@{url}:5672")
    c.conf.result_backend = "rpc://"
    c.conf.result_persistent = True
    config_api_urls = config.get("api_urls", {})
    api_urls = APIUrls(
        device_management=config_api_urls.get(
            "device_management", "http://localhost:8123/api/v1/"),
        server_internal=config_api_urls.get(
            "server_internal", "http://localhost:8010/api/v1/"),
        server_external=config_api_urls.get(
            "server_external", "http://localhost:8011/api/v1/"),
        rstuf=config_api_urls.get(
            "rstuf", "http://localhost/api/v1/"),
        wfx_south=config_api_urls.get(
            "wfx_south", "http://localhost:8090/api/wfx/v1/"),
        wfx_north=config_api_urls.get(
            "wfx_north", "http://localhost:8091/api/wfx/v1/"),
    )
    bot = Bot(api_urls)

    @c.task(bind=True)
    def update_with_wfx(**kwargs: dict) -> tuple[bool, str | None]:  # noqa: ARG001
        return True , "dummy"

if __name__ == "__main__":
    app = create_app(bot, update_with_wfx)
    app.run(debug=True, extra_files=["static/main.scss"])
