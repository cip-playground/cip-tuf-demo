local tufclient = require("tufclient")

local error_types = {
    ErrConfigMissing         = 1,
    ErrTypeReadRoot          = 2,
    ErrTypeInitMetadata      = 3,
    ErrFailedCreateUpdater   = 4,
    ErrFailedRefreshMetadata = 5,
    ErrTargetMissing         = 6,
    ErrTargetVersionMissing  = 7,
    ErrTargetVersionOlder    = 8,
    ErrFailedDownloadTarget  = 9
}

local client_config = {
    MetadataBaseURL = "http://localhost:8011/api/v1/metadata",
    TargetsBaseURL = "http://localhost:8011/api/v1/artifacts",
    LocalMetadataDir = "../sample/metadata",
    CurrentVersion = "1.0.0"
}

local r = tufclient.setup(client_config)

if r == 0 then
    print("Success to set updater")
elseif r == error_types.ErrTypeReadRoot then
    print("Error: Failed to read root.json")
    os.exit(1)
elseif r == error_types.ErrTypeInitMetadata then
    print("Error: Failed to initialize trusted metadata")
    os.exit(1)
elseif r == error_types.ErrConfigMissing then
    print("Error: Missing configuration")
    os.exit(1)
else
    print("Unknown error:", r)
    os.exit(1)
end

r = tufclient.download_target("group1.swu")

if r ~= 0 then
    print("Return value:", r)
elseif r == 0 then
    print("Success to download target file")
end
