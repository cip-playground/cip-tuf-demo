package main

/*
#cgo CFLAGS: -I/usr/include/lua5.4
#cgo LDFLAGS: -L/usr/lib -llua5.4
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "stdlib.h"

extern int Setup(lua_State *L);
extern int DownloadTarget(lua_State *L);

static const luaL_Reg tufclient[] = {
    {"setup", Setup},
    {"download_target", DownloadTarget},
    {NULL, NULL} };

static void myLuaL_newlib(lua_State *L) {
    luaL_newlib(L, tufclient);
}
*/
import "C"
import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"unsafe"

	"github.com/Masterminds/semver/v3"
	"github.com/theupdateframework/go-tuf/v2/metadata/config"
	"github.com/theupdateframework/go-tuf/v2/metadata/trustedmetadata"
	"github.com/theupdateframework/go-tuf/v2/metadata/updater"
)

type TargetCustomData struct {
	Version string `json:"version"`
}

type ErrorType int
const (
	ErrConfigMissing ErrorType = iota + 1
	ErrTypeReadRoot
	ErrTypeInitMetadata
	ErrFailedCreateUpdater
	ErrFailedRefreshMetadata // 5
	ErrTargetMissing
	ErrTargetVersionMissing
	ErrTargetVersionOlder
	ErrFailedDownloadTarget
							 // 10
)
type CustomError struct {
	Type    ErrorType
	Message string
}
func (e *CustomError) Error() string {
	return e.Message
}

type ClientConfig struct {
	MetadataBaseURL  string
	LocalMetadataDir string
	LocalTargetDir   string
	TargetsBaseURL   string
	CurrentVersion   string
}
type ConfigManager struct {
	config *ClientConfig
}
func (cm *ConfigManager) setConfig(config *ClientConfig) {
	cm.config = config
}
func (cm *ConfigManager) getConfig() *ClientConfig {
	return cm.config
}
func (cm *ConfigManager) readAndValidateRootJSON() ([]byte, *CustomError) {
	rootJSONPath := filepath.Join(cm.config.LocalMetadataDir, "root.json")
	rootJSONBytes, err := os.ReadFile(rootJSONPath)

	if err != nil {
		return nil, &CustomError{
			Type:    ErrTypeReadRoot,
			Message: fmt.Sprintf("Failed to read root.json: %v", err),
		}
	}

	var what *trustedmetadata.TrustedMetadata
	what, err = trustedmetadata.New(rootJSONBytes)
	if err != nil && what.Root != nil {
		return nil, &CustomError{
			Type:    ErrTypeInitMetadata,
			Message: fmt.Sprintf("root.json is not in the correct format: %v", err),
		}
	}
	return rootJSONBytes, nil
}

func (cm *ConfigManager) createTUFUpdater() (*updater.Updater, *CustomError) {
	rootJSONBytes, customErr := cm.readAndValidateRootJSON()
	if customErr != nil {
		return nil, customErr
	}

	tufConfig, err := config.New(cm.config.MetadataBaseURL, rootJSONBytes)
	if err != nil {
		return nil, &CustomError{
			Type:    ErrTypeInitMetadata,
			Message: "Failed to create Updater instance",
		}
	}

	tufConfig.LocalMetadataDir = cm.config.LocalMetadataDir
	tufConfig.LocalTargetsDir = cm.config.LocalTargetDir
	tufConfig.RemoteTargetsURL = cm.config.TargetsBaseURL

	tufUpdater, err := updater.New(tufConfig)
	if err != nil {
		return nil, &CustomError{
			Type:    ErrTypeInitMetadata,
			Message: fmt.Sprintf("Failed to initialize Updater: %v", err),
		}
	}

	err = tufUpdater.Refresh()
	if err != nil {
		return nil, &CustomError{
			Type:    ErrFailedRefreshMetadata,
			Message: fmt.Sprintf("Failed to refresh metadata: %v", err),
		}
	}

	return tufUpdater, nil
}

var (
	configManager  = &ConfigManager{}
)

func getField(L *C.lua_State, fieldName string) (string, *CustomError) {
	var len C.size_t
	cFieldName := C.CString(fieldName)
	defer C.free(unsafe.Pointer(cFieldName))
	C.lua_getfield(L, 1, cFieldName)
	if C.lua_type(L, -1) == C.LUA_TNIL {
		return "", &CustomError{
			Type:    ErrConfigMissing,
			Message: fmt.Sprintf("%s is missing", fieldName),
		}
	}
	value := C.GoString(C.lua_tolstring(L, -1, &len))
	C.lua_settop(L, -2)
	return value, nil
}

func retrieveConfig(L *C.lua_State) (*ClientConfig, *CustomError) {
	fields := []string{"MetadataBaseURL", "TargetsBaseURL", "LocalTargetDir", "LocalMetadataDir", "CurrentVersion"}
	values := make(map[string]string)

	for _, field := range fields {
		value, err := getField(L, field)
		if err != nil && field != "LocalTargetDir" && field != "LocalMetadataDir" && field != "CurrentVersion" {
			return nil, err
		}
		values[field] = value
	}

	if values["LocalTargetDir"] == "" {
		values["LocalTargetDir"] = "targets"
	}
	if values["LocalMetadataDir"] == "" {
		values["LocalMetadataDir"] = "metadata"
	}

	return &ClientConfig{
		MetadataBaseURL:  values["MetadataBaseURL"],
		LocalMetadataDir: values["LocalMetadataDir"],
		LocalTargetDir:   values["LocalTargetDir"],
		TargetsBaseURL:   values["TargetsBaseURL"],
		CurrentVersion:   values["CurrentVersion"],
	}, nil
}

//export Setup
func Setup(L *C.lua_State) C.int {
	config, customErr := retrieveConfig(L)
	if customErr != nil {
		C.lua_pushinteger(L, C.longlong(customErr.Type))
		return 1
	}
	configManager.setConfig(config)
	_, customErr = configManager.createTUFUpdater()
	if customErr != nil {
		C.lua_pushinteger(L, C.longlong(customErr.Type))
		return 1
	}
	C.lua_pushinteger(L, 0)
	return 1
}

//export DownloadTarget
func DownloadTarget(L *C.lua_State) C.int {
	updater, customErr := configManager.createTUFUpdater()
	if customErr != nil {
		C.lua_pushinteger(L, C.longlong(customErr.Type))
		return 1
	}
	var len C.size_t
	targetName := C.GoString(C.lua_tolstring(L, 1, &len))

	targetInfo, err := updater.GetTargetInfo(targetName)
	if targetInfo == nil || err != nil {
		C.lua_pushinteger(L, C.longlong(ErrTargetMissing))
		return 1
	}

	var targetData TargetCustomData
	if err := json.Unmarshal(*targetInfo.Custom, &targetData); err != nil {
		C.lua_pushinteger(L, C.longlong(ErrTargetVersionMissing))
		return 1
	}

	v1, err1 := semver.NewVersion(configManager.getConfig().CurrentVersion)
	v2, err2 := semver.NewVersion(targetData.Version)

	if err1 != nil || err2 != nil {
		C.lua_pushinteger(L, C.longlong(ErrTargetVersionMissing))
		return 1
	}

	if v2.LessThanEqual(v1) {
		C.lua_pushinteger(L, C.longlong(ErrTargetVersionOlder))
		return 1
	}

	_, data, err := updater.DownloadTarget(targetInfo, "", "")
	if err != nil {
		C.lua_pushinteger(L, C.longlong(ErrFailedDownloadTarget))
		return 1
	}
	_ = data
	data = nil

	C.lua_pushinteger(L, 0)
	return 1
}

//export luaopen_tufclient
func luaopen_tufclient(L *C.lua_State) C.int {
	C.myLuaL_newlib(L)
	return 1
}

func main() {}
