
version: "3.9"

volumes:
  metadata-asdirectory:
  artifacts-asdirectory:
  pgsql-data:
  mq-data:
  wfx-db:
  rstuf-api-data:
  rstuf-redis-data:
  rstuf-storage:


services:
  postgres:
    image: postgres:15.1
    ports:
      - "5433:5432"
    environment:
      - POSTGRES_PASSWORD=secret
    volumes:
      - "pgsql-data:/var/lib/postgresql/data"
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "postgres", "-d", "postgres"]
      interval: 1s

  pgweb:
    image: sosedoff/pgweb:0.15.0
    ports:
      - "8082:8081"
    environment:
      - PGWEB_DATABASE_URL=postgresql://postgres:secret@postgres:5432/postgres?sslmode=disable
    depends_on:
      - postgres

  repository-service-tuf-api:
    image: ghcr.io/repository-service-tuf/repository-service-tuf-api:v0.14.0b1
    volumes:
      - rstuf-api-data:/data
    ports:
      - 80:80
    environment:
      - RSTUF_BROKER_SERVER="amqp://guest:guest@rabbitmq:5672"
      - RSTUF_REDIS_SERVER=redis://redis
    depends_on:
      redis:
        condition: service_healthy

  rabbitmq:
    image: rabbitmq:latest
    ports:
      - 5672:5672
      - 15672:15672
    volumes:
     - "mq-data:/var/lib/rabbitmq"
    healthcheck:
      test: "exit 0"

  web:
    image: s3rver:latest
    volumes:
      - rstuf-storage:/app/metadata
      - artifacts-asdirectory:/app/artifacts
    ports:
      - "8011:8010"
    environment:
      - DATABASE_URL=postgresql://postgres:secret@postgres:5432/postgres
    entrypoint:
      - s3rver
      - --artifact-dir
      - /app/artifacts
      - --metadata-dir
      - /app/metadata
      - --host
      - "0.0.0.0"
    depends_on:
      - postgres

  redis:
    image: redis:4.0
    volumes:
      - rstuf-redis-data:/data
    ports:
      - "6379:6379"
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 1s

  repository-service-tuf-worker:
    image: ghcr.io/repository-service-tuf/repository-service-tuf-worker:v0.16.0b1
    environment:
      - RSTUF_STORAGE_BACKEND=LocalStorage
      - RSTUF_KEYVAULT_BACKEND=LocalKeyVault
      - RSTUF_LOCAL_STORAGE_BACKEND_PATH=/var/opt/repository-service-tuf/storage
      - RSTUF_LOCAL_KEYVAULT_PATH=/var/opt/repository-service-tuf/key_storage
      - RSTUF_LOCAL_KEYVAULT_KEYS=online,1234
      - RSTUF_BROKER_SERVER=amqp://guest:guest@rabbitmq:5672
      - RSTUF_REDIS_SERVER=redis://redis
      - RSTUF_SQL_SERVER=postgresql://postgres:secret@postgres:5432
      - RSTUF_WORKER_ID=dev
    volumes:
      - rstuf-storage:/var/opt/repository-service-tuf/storage
      - ./sample/keys/:/var/opt/repository-service-tuf/key_storage
    depends_on:
      redis:
        condition: service_healthy
      postgres:
        condition: service_healthy
    tty: true
    stdin_open: true

  rstuf-ft-runner:
    image: python:3.10-slim-buster
    command: python -V
    working_dir: /rstuf-runner
    volumes:
      - ./:/rstuf-runner

  wfx:
    image: ghcr.io/siemens/wfx:latest
    ports:
      - "8090:8090"
      - "8091:8091"
    volumes:
      - wfx-db:/home/nonroot
    environment:
      - PGHOST=postgres
      - PGPORT=5432
      - PGUSER=postgres
      - PGPASSWORD=secret
      - PGDATABASE=postgres
    depends_on:
      - postgres
    entrypoint:
      - wfx
      - --mgmt-port
      - '8091'
      - --client-port
      - '8090'
      - --storage
      - postgres

  mosquitto:
    image: eclipse-mosquitto
    container_name: mymosquitto
    ports:
      - "1883:1883"
    volumes:
      - ./heartbeat/mosquitto.conf:/mosquitto/config/mosquitto.conf
