-- Import the suricatta module
local suricatta = require("suricatta")
local tufclient = require("tufclient")
local _ , heartbeat = pcall(require, "heartbeat")

local tufclient_error_type = {
    ErrConfigMissing         = 1,
    ErrTypeReadRoot          = 2,
    ErrTypeInitMetadata      = 3,
    ErrFailedCreateUpdater   = 4,
    ErrFailedRefreshMetadata = 5,
    ErrTargetMissing         = 6,
    ErrTargetVersionMissing  = 7,
    ErrTargetVersionOlder    = 8,
    ErrFailedDownloadTarget  = 9
}

--- Checks if a value is a table.
--
--- @param  value any     The value to check.
--- @return       boolean # True if the value is a table, false otherwise.
local function is_table(value)
    -- Check the type of the value
    return type(value) == "table"
end


--- Merges two tables recursively, with values from the second table overriding the first.
--
--- @param  base_table     table The base table to merge into.
--- @param  override_table table The table containing values to override the base table.
--- @return                table # The resulting merged table.
local function merge_tables(base_table, override_table)
    -- Iterate over the keys and values in the override table
    for key, value in pairs(override_table) do
        -- If the value is a table, perform a recursive merge
        if is_table(value) then
            -- If both tables have a table for this key, merge them
            if is_table(base_table[key]) then
                merge_tables(base_table[key], value)
            else
                -- Otherwise, create a new subtable and merge
                base_table[key] = merge_tables({}, value)
            end
        else
            -- If the value is not a table, simply overwrite the key
            if base_table[key] ~= value then
                base_table[key] = value
            end
        end
    end
    -- Return the merged table
    return base_table
end


--- Retrieves the version string from a file.
--
--- @param  file_path string     The path to the file containing the version string.
--- @param  pattern   string|nil     The pattern to search for in the file, uses default if nil.
--- @return           string|nil # The extracted version string, or nil if not found.
local function get_version(file_path, pattern)
    suricatta.notify.debug("[get_version] Reading version from file: " .. file_path .. " with pattern: " .. pattern)

    -- Open the file
    local file_handle = io.open(file_path, "r")

    -- If the file does not exist, log an error and return nil
    if not file_handle then
        suricatta.notify.error("[get_version] File not found at path: " .. file_path)
        return nil
    end

    -- Read the entire contents of the file
    local file_content = file_handle:read("*all")

    -- If the file could not be read, log an error and return nil
    if file_content == nil then
        suricatta.notify.error("[get_version] Failed to read file at path: " .. file_path)
        file_handle:close()
        return nil
    end

    -- Search for the version string within the file
    local version_match = file_content:match(pattern)

    -- If the version string could not be found, log an error and return nil
    if version_match == nil then
        suricatta.notify.error("[get_version] Failed to find version string in file: " .. file_path)
        return nil
    end

    -- Close the file
    file_handle:close()

    -- Return the version string
    return version_match
end


--- Definitions for states
local state = {
    --- A new state of the workflow
    NEW = "NEW",

    --- A state indicating that the update image is downloaded
    DOWNLOADED = "DOWNLOADED",

    --- A state indicating that the update image is installed
    INSTALLED = "INSTALLED",

    --- A successful terminal state of the workflow
    SUCCESS = "SUCCESS",

    --- A failed terminal state of the workflow
    FAILED = "FAILED",
}


--- State management class
StateManager = {}
StateManager.__index = StateManager


--- Constructor
--
--- @return table # StateManager A new instance of StateManager
function StateManager:new()
    local obj = {
        url = nil,
        job_id = nil,
        channel = nil
    }
    setmetatable(obj, self)
    return obj
end


--- Method to get job ID
--
--- @param  id string        The client ID to use for fetching the job ID
--- @return    nil | boolean # True if the job ID was fetched successfully, false otherwise
function StateManager:getJobID(id)
    if self.url == nil then
        return nil
    end

    -- TODO: What to do if there are multiple jobs with GROUP=OPEN
    local url_to_check_id = (
        "%s/jobs?clientId=%s&group=OPEN&limit=1"):format(self.url, id)
    local res, status, data = self.channel.get {url=url_to_check_id}
    if res and suricatta.status.OK then
        local job_id = data.json_reply.content[1].id
        self.job_id = job_id
        suricatta.notify.debug("[StateManager:getJobID] Job ID: " .. job_id)
        return true
    else
        suricatta.notify.error(
            "[StateManager:getJobID] Failed to get job ID. Status: " .. tostring(status))
        return false
    end
end


--- Check if there is a specific job with a certain state
--
--- @param  id             string        The client ID to use for fetching the job ID
--- @param  expected_state string        The expected state of the job
--- @return nil | boolean                # Result of the check
function StateManager:checkeJobState(id, expected_state)
    if self.url == nil then
        return nil
    end

    local function isTableEmpty(tbl)
        if tbl == nil then
            return true
        end
        return next(tbl) == nil
    end

    local url_to_check_id = (
        "%s/jobs?clientId=%s&state=%s"):format(self.url, id, expected_state)
    local res, status, data = self.channel.get {url=url_to_check_id}
    if res and suricatta.status.OK then
        if isTableEmpty(data.json_reply.content) then
            return false
        end
        return true
    else
        suricatta.notify.error(
            "[StateManager:checkeJobState] Failed to get jobs: " .. tostring(status))
        return false
    end
end


--- Method to update job status
--
--- @param  new_state string        The new state to update the job to
--- @return           nil | boolean # True if the job status was updated successfully, false otherwise
function StateManager:updateJobStatus(new_state)
    if self.job_id == nil then
        return
    end
    local url_to_update_status = (
        "%s/jobs/%s/status"):format(self.url, self.job_id)

    local res, status, operation_result = self.channel.put {
        url = url_to_update_status,
        content_type = "application/json",
        method = suricatta.channel.method.PUT,
        request_body = ('{"state": "%s"}'):format(new_state),
    }

    if res and suricatta.status.OK then
        suricatta.notify.info("[StateManager:updateJobStatus] Job status updated to " .. new_state)
        return true
    else
        suricatta.notify.error(
            ("[StateManager:updateJobStatus] Failed to update job status. Status: %s. HTTP_STATUS: %s."):format(
                status, operation_result.http_response_code))
        return false
    end
end


-- Method to close channel
--
--- @return nil
function StateManager:close()
    if self.job_id ~= nil then
        self.job_id = nil
        self.channel.close()
        self.channel = nil
    end
end


-- Global configuration and state values
local global_values = {
    config={},
    channel={},
    progress = StateManager:new(),
    version_info={},
    heartbeating = false
}


--- Checks if there is a pending update action.
--
--- @param  action_identifier number           The identifier for the action.
--- @return                   number           # The action identifier.
--- @return                   suricatta.status # The status code indicating update availability.
local function has_pending_action(action_identifier)
    if global_values.heartbeating then
        local r = heartbeat.send("0")
        if r == 1 then
            suricatta.notify.debug("[has_pending_action] Success to send heartbeat")
        else
            suricatta.notify.warn("[has_pending_action] Failed to send heartbeat")
        end
    end

    -- Get the current update state
    local update_state = suricatta.pstate.get()

    -- If the update state is INSTALLED or TESTING, return no update available
    if (update_state == suricatta.pstate.INSTALLED) or (update_state == suricatta.pstate.TESTING) then
        return action_identifier, suricatta.status.NO_UPDATE_AVAILABLE
    end

    -- Formulate the target file name based on the configuration ID
    local target_file = global_values.version_info.target_filename

    suricatta.notify.info("[has_pending_action] Start to download target file")
    local r = tufclient.download_target(target_file)

    if r == 0 then
        suricatta.notify.info("[has_pending_action] Success to download target file")
    else
        if r == tufclient_error_type.ErrTargetMissing then
            suricatta.notify.info("[has_pending_action] Target file missing")
        else
            suricatta.notify.error("[has_pending_action] Unknown error: %d", r)
        end
        return action_identifier, suricatta.status.NO_UPDATE_AVAILABLE
    end

    -- Get the job ID if progress URL is set
    local res = global_values.progress:getJobID(global_values.config.id)
    if (res ~= nil) and (not res) then
        return action_identifier, suricatta.status.NO_UPDATE_AVAILABLE
    end

    -- Update the job status to DOWNLOADED
    local res = global_values.progress:updateJobStatus(state.DOWNLOADED)
    if (res ~= nil) and (not res) then
        return action_identifier, suricatta.status.NO_UPDATE_AVAILABLE
    end

    return action_identifier, suricatta.status.UPDATE_AVAILABLE
end
-- Register the HAS_PENDING_ACTION callback
suricatta.server.register(has_pending_action, suricatta.server.HAS_PENDING_ACTION)


--- Installs an update.
--
--- @return suricatta.status # The status code of the update installation.
local function install_update()
    -- Formulate the target file name based on the configuration ID
    local target_file = global_values.version_info.target_filename

    -- Formulate the full path to the destination file
    local destination_file = ("%s%s"):format(suricatta.get_tmpdir(), target_file)

    -- Create a file URL for the destination file
    local file_url = ("file://%s"):format(destination_file)

    -- Attempt to install the update using the provided file URL
    local install_success, _, _ = suricatta.install({ channel = global_values.channel, url = file_url, drain_messages = false })

    -- If the destination file exists, remove it
    if destination_file then
        suricatta.notify.debug("[install_update] Removing destination file: " .. destination_file)
        os.remove(destination_file)
    end

    -- If the installation was successful, return the OK status code
    if install_success then
        local res = global_values.progress:updateJobStatus(state.INSTALLED)
        if (res ~= nil) and (not res) then
            return suricatta.status.EERR
        end
        global_values.progress:close()
        suricatta.notify.info("[install_update] Installation success")
        return suricatta.status.OK
    else
        -- If the installation failed, log an error and return an error status code
        local res = global_values.progress:updateJobStatus(state.FAILED)
        if (res ~= nil) and (not res) then
            return suricatta.status.EERR
        end
        global_values.progress:close()
        suricatta.notify.error("[install_update] Installation failed for file URL: " .. file_url)
        return suricatta.status.EERR
    end
end
-- Register the INSTALL_UPDATE callback
suricatta.server.register(install_update, suricatta.server.INSTALL_UPDATE)


--- Retrieves the polling interval from the configuration.
--
--- @return number # The polling interval.
local function get_polling_interval()
    -- Return the polling interval from the configuration
    return global_values.config.polldelay
end
-- Register the GET_POLLING_INTERVAL callback
suricatta.server.register(get_polling_interval, suricatta.server.GET_POLLING_INTERVAL)


--- Callback function for progress updates.
--
--- @param  progress_message suricatta.ipc.progress_msg The progress message to be processed.
--- @return                  suricatta.status           # The status code indicating the result of the callback processing.
local function progress_callback(progress_message)
    -- Currently, this function simply returns the OK status code
    -- Assuming that progress status is updated on the server.
    return suricatta.status.OK
end
-- Register the CALLBACK_PROGRESS callback
suricatta.server.register(progress_callback, suricatta.server.CALLBACK_PROGRESS)


--- Starts the server with the given configurations.
--
--- @param  default_config table            The default configuration.
--- @param  argv           table            The command-line arguments passed to the server.
--- @param  user_config    table            The user-provided configuration.
--- @return                suricatta.status # The status code indicating the result of the server start operation.
local function server_start(default_config, argv, user_config)
    -- Merge the default and user-provided configurations
    global_values.config = merge_tables(default_config, user_config)

    local version_file_path = nil
    local group_id = nil
    local version_pattern = 'BCP_VERSION="([^"]+)"'
    local progress_url = nil
    local update_state = nil
    local broker = nil
    for i = 1, #argv do
        if argv[i] == "--group-id" and argv[i+1] then
            group_id = argv[i+1]
        elseif argv[i] == "--version-path" and argv[i+1] then
            version_file_path = argv[i+1]
        elseif argv[i] == "--version-pattern" and argv[i+1] then
            version_pattern = argv[i+1]
        elseif argv[i] == "--progress-url" and argv[i+1] then
            progress_url = argv[i+1]
        elseif argv[i] == "--update-state" and argv[i+1] then
            update_state = argv[i+1]
        elseif argv[i] == "--broker" and argv[i+1] then
            broker = argv[i+1]
        end
    end

    -- If progress URL is set, open a channel for sharing status and store it in the global values
    if progress_url ~= nil then
        global_values.progress.url = progress_url
        suricatta.notify.debug(("progress_url: %s"):format(global_values.progress.url))
        local operation_result, channel = suricatta.channel.open({})
        if not operation_result then
            suricatta.notify.warn("[server_start] Failed to return channel instance")
        end
        global_values.progress.channel = channel
    end

    -- If update state is set, update the job status
    if update_state ~= nil then
        local res = global_values.progress:getJobID(global_values.config.id)
        if (res ~= nil) and (not res) then
            suricatta.notify.error("[server_start] Failed to get job ID")
            return suricatta.status.EINIT
        end
        local res = global_values.progress:updateJobStatus(update_state)
        if (res ~= nil) and (not res) then
            suricatta.notify.error("[has_pending_action] Failed to update job status to " .. update_state)
            return suricatta.status.EINIT
        end
        return suricatta.status.UPDATE_CANCELED
    end

    if group_id ~= nil then
        global_values.version_info.target_filename = ("%s.swu"):format(group_id)
    else
        global_values.version_info.target_filename = ("%s.swu"):format(global_values.config.id)
    end
    local device_identifier = global_values.config.id

    -- Retrieve the device ID and server URL from the merged configuration
    local server_url = global_values.config.url

    -- If the device ID or server URL are not set, log an error and return an error status code
    if device_identifier == nil then
        suricatta.notify.error("[server_start] Device ID is not set in configuration")
        return suricatta.status.EINIT
    elseif server_url == nil then
        suricatta.notify.error("[server_start] Server URL is not set in configuration")
        return suricatta.status.EINIT
    elseif version_file_path == nil then
        suricatta.notify.error("[server_start] Path to version is not set as option (--version-path)")
        return suricatta.status.EINIT
    end

    global_values.version_info.file_path = version_file_path
    global_values.version_info.pattern = version_pattern

    -- Attempt to open a channel to the server
    local channel_open_result, new_channel = suricatta.channel.open({url=server_url})
    -- If the channel could not be created, log an error and return an error status code
    if not channel_open_result then
        suricatta.notify.error("[server_start] Failed to create channel instance for URL: " .. server_url)
        return suricatta.status.EINIT
    end

    -- Retrieve the current version string from the version file
    local current_version_str = get_version(global_values.version_info.file_path, global_values.version_info.pattern)
    -- If the current version string could not be retrieved, log an error and return no update available
    if current_version_str == nil then
        suricatta.notify.error("[server_start] Failed to retrieve current version string")
        return suricatta.status.EINIT
    end

    -- Store the new channel in the global values
    global_values.channel = new_channel

    local client_config = {
        MetadataBaseURL = global_values.config.url .. "/metadata",
        TargetsBaseURL = global_values.config.url .. "/artifacts",
        LocalTargetDir = suricatta.get_tmpdir(),
        CurrentVersion = current_version_str
    }

    local r = tufclient.setup(client_config)

    if r == 0 then
        suricatta.notify.info("[server_start] Success to set updater")
    else
        if r == tufclient_error_type.ErrTypeReadRoot then
            suricatta.notify.error("[server_start] Error: Failed to read root.json")
        elseif r == tufclient_error_type.ErrTypeInitMetadata then
            suricatta.notify.error("[server_start] Error: Failed to initialize trusted metadata")
        elseif r == tufclient_error_type.ErrConfigMissing then
            suricatta.notify.error("[server_start] Error: Missing configuration")
        else
            suricatta.notify.error("[server_start] Unknown error: %d", r)
        end
        return suricatta.status.EINIT
    end

    if broker ~= nil then
        local config = {
            broker = broker,
            clientID = device_identifier,
        }
        local r = heartbeat.set_config(config)
        if r == 1 then
            suricatta.notify.info("[server_start] Success to set heartbeat")
            local r = heartbeat.send("0")
            if r == 1 then
                suricatta.notify.debug("[server_start] Success to send heartbeat")
            else
                suricatta.notify.error("[server_start] Failed to send heartbeat")
                return suricatta.status.EINIT
            end
            global_values.heartbeating = true
        else
            suricatta.notify.error("[server_start] Failed to set heartbeat")
            return suricatta.status.EINIT
        end
    end

    -- Check if there are any residual SWU files and attempt to remove them
    local temp_file_path = ("%s%s"):format(suricatta.get_tmpdir(), global_values.version_info.target_filename)
    local temp_file_handle = io.open(temp_file_path, "r")
    if temp_file_handle then
        temp_file_handle:close()
        local remove_success, remove_error_msg = os.remove(temp_file_path)
        if not remove_success then
            suricatta.notify.error(("[server_start] Failed to remove residual file: %s. Error: %s"):format(temp_file_path, remove_error_msg))
            return suricatta.status.EINIT
        end
    end

    -- Return the OK status code indicating successful server start
    return suricatta.status.OK
end
-- Register the SERVER_START callback
suricatta.server.register(server_start, suricatta.server.SERVER_START)


--- Stops the server.
--
--- @return suricatta.status # The status code indicating the result of the server stop operation.
local function server_stop() --  TODO: When will it run?
    -- Currently, this function simply returns the OK status code
    return suricatta.status.OK
end
-- Register the SERVER_STOP callback
suricatta.server.register(server_stop, suricatta.server.SERVER_STOP)


--- Prints help information.
--
--- @param  defaults table|nil        The default values to be used in the help information.
--- @return          suricatta.status # The status code indicating the result of the help operation.
local function print_help(defaults)
    -- Currently, this function simply returns the EERR status code
    defaults = defaults or {}
    return suricatta.status.EERR
end
-- Register the PRINT_HELP callback
suricatta.server.register(print_help, suricatta.server.PRINT_HELP)


--- Sends the target data.
--
--- @return suricatta.status # The status code indicating the result of the send target data operation.
local function send_target_data()
    -- Currently, this function simply returns the OK status code
    return suricatta.status.OK
end
-- Register the SEND_TARGET_DATA callback
suricatta.server.register(send_target_data, suricatta.server.SEND_TARGET_DATA)


--- IPC (Inter-Process Communication) function.
--
--- @param  ipc_message string           The IPC message to be processed.
--- @return             string           # A response to the IPC message.
--- @return             suricatta.status # The status code indicating the result of the IPC communication.
local function ipc(ipc_message)
    -- This function is out of action because client-tuf is responsible for downloading the SWU file.
    return 'hello in ipc', suricatta.status.OK
end
-- Register the IPC callback
suricatta.server.register(ipc, suricatta.server.IPC)
