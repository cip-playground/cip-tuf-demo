"use client"

import { Software } from "../../components/software"

export default function Page({ params }: { params: { slug: string } }) {
  return (
    <main>
        <div className='text-xl'>{params.slug}</div>
        <hr className="mb-5"/>
        {Software(params.slug)}
    </main>
  )
}
