"use client"

import React, { useState, useEffect } from 'react';

import { getData } from '../fetch/page';

export default function Counter(){
    const [count, setCount] = useState(2);

    useEffect(() => {
        const intervalId = setInterval(() => {
            setCount(prevCount => {
                console.log("prev")
                return prevCount + 1});
        }, 10000)
    return () => clearInterval(intervalId)
    }, []);

    return <div>{count}</div>
}
