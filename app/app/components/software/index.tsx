import React, { useEffect, useState, useRef } from "react"


function handleActivity(data) {
    let components = []
    const content = data.content
    if (content === undefined) {
        return <ul></ul>
    }
    let jobId, mtime, state, message, version
    for (let i = 0; i < content.length; i++) {
        jobId = content[i].id
        mtime = new Date(content[i].mtime)
        mtime = mtime.toLocaleDateString('ja-JP', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            timeZone: 'Asia/Tokyo'
        });
        state = content[i].status.state
        message = content[i].status.message
        if (state === "TERMINATED") {
            state = "🔴"
        } else if (state === "ACTIVATED") {
            state = "🟢"
        } else {
            state = "⚪"
        }
        version = data.content[i].definition.artifacts[0].version
        components[i] = { "jobID": jobId, "mtime": mtime, "state": state, "message": message, "version": version }
    }

    return (<div>
        <ul>
            {components.map((component) => (
                <li key={component.mtime}>{component.state} {component.version} {component.mtime} {component.message}</li>
            ))}
        </ul>
    </div>)
}

function createStagedData(data) {
    if (Object.keys(data).length === 0) {
        return {}
    }
    const data22 = data[0]
    const jobId = data22.id
    let mtime = new Date(data22.mtime)
    mtime = mtime.toLocaleDateString('ja-JP', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        timeZone: 'Asia/Tokyo'
    });
    const message = data22.status.message === undefined ? '' : data22.status.message
    const version = data22.definition.artifacts[0].version
    return { "jobId": jobId, "version": version, "progress": data22.status.progress, "state": data22.status.state, "stateIcon": "⚪", "message": message, "mtime": mtime }
}

function handleStaged(data) {
    console.log(data)
    return (
        <div className="flex flex-col text-sm pt-2 pb-2">
            <div className="flex text-center rounded">
                <div className="basis-1/6 text-center pr-4">[{data.state}]</div>
                <div className="basis-1/6 text-left pr-4">{data.version}</div>
                <div className="basis-4/6 text-left">{data.mtime}</div>
            </div>
            <div className="flex flex-row">
                <div className="basis-1/6 text-center pl-4 pr-6">{data.progress !== "" ? `${data.progress}%` : '-'}</div>
                <div className="basis-5/6 text-left">{data.message !== "" ? data.message : 'no msg'}</div>
            </div>
        </div>
    )

}

export const Software = (deviceID: string) => {
    // [Software]
    const [existsSoftware, setExistsSoftware] = useState(false)
    const [isDeletingSoftware, setIsDeletingSoftware] = useState(false);
    const [isUploadingSoftware, setIsUploadingSoftware] = useState(false);
    const softwareInputRef = useRef<HTMLInputElement | null>(null);
    const [software, setSoftware] = useState<File | null>(null);
    const [softwareVersion, setSoftwareVersion] = useState<string | null>(null);
    const [softwareHashes, setSoftwareHashes] = useState("");
    const [softwareLength, setSoftwareLength] = useState(0);

    // [Job]
    const [isSendingJobReq, setIsSendingJobReq] = useState(false);
    const [jobId, setJobId] = useState("")
    const [isStagingJob, setIsStagingJob] = useState(false);
    const [jobStatusData, setJobStatusData] = useState({});
    const [jobActivityData, setJobActivityData] = useState({});
    const [statusJobDownload, setStatusJobDownload] = useState(false);
    const [statusJobInstall, setStatusJobInstall] = useState(false);

    // [All]
    const [reload, setReload] = useState(false);


    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const files = event.target.files;
        if (files && files.length > 0) {
            setSoftware(files[0])
        }
    }

    const handleUploadSoftware = async () => {
        if (software === null || softwareVersion === null) return;

        // TODO: Check whether semantic versioning is used. || Changes to how versions are entered.

        setIsUploadingSoftware(true)

        const formData = new FormData();
        formData.append("file", software, deviceID + ".swu")
        formData.append("version", softwareVersion)
        try {
            console.log("Upload software to file-server.")
            const responseFileServer = await fetch("http://XXX.XXX.XXX.XXX:58001/upload", {
                method: "POST",
                body: formData,
            });
            const data = await responseFileServer.json();
            console.log(data);

            if (responseFileServer.ok) {
                console.log("Upload to file-server is success.")
            } else {
                console.log("Upload to file-server is failed")
                setIsUploadingSoftware(false)
                return;
            }

            console.log("Register software to metadata-server.")
            const responseMetadata = await fetch("http://XXX.XXX.XXX.XXX:58002/upload", {
                method: "POST",
                body: formData,
            });
            const data2 = await responseMetadata.json()
            console.log(data2)
            if (responseMetadata.ok) {
                console.log("Register in metadata-server is success.")
                setSoftware(null);
                setSoftwareVersion(null)
                if (softwareInputRef.current) {
                    softwareInputRef.current.value = "";
                }
            } else {
                console.log("Register in metadata-server is failed.")
                setIsUploadingSoftware(false)
                return;
            }
            setReload(!reload)
            setExistsSoftware(true)
        } catch (error) {
            console.error("File upload failed", error)
        } finally {
            setIsUploadingSoftware(false)
        }
    }

    const handleVersionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSoftwareVersion(event.target.value)
    }

    function handleClickUploadButton() {
        if (softwareInputRef.current && typeof softwareInputRef.current.click === 'function') {
            softwareInputRef.current.click();
        }
    }

    const componentUpload = (
        <div className="flex flex-row text-sm">
            <div className="basis-1/2 flex border border-black rounded">
                <button className="basis-1/2 bg-yellow-200 p-2" disabled={isUploadingSoftware} onClick={handleClickUploadButton}>Select Software</button>
                <div className="basis-1/2 p-2 cursor-default text-center">{software !== null ? software.name : "Not selected yet."}</div>
            </div>
            <input className="basis-1/8 ml-auto p-2 border border-black text-center rounded" type="text" onChange={handleVersionChange} disabled={isUploadingSoftware} placeholder='Enter version' />
            <button className="basis-1/8 ml-auto p-2 border border-black rounded" onClick={handleUploadSoftware} disabled={isUploadingSoftware}>{isUploadingSoftware ? "Uploading..." : "Upload"}</button>
            <input className="hidden" type="file" onChange={handleFileChange} ref={softwareInputRef} />
        </div>
    )

    const handleJobSubmit = async () => {
        setIsSendingJobReq(true)
        const requestBody = {
            clientID: deviceID,
            workflow: "wfx.workflow.dau.phased",
            definition: {
                version: 'v1',
                type: ["firmware"],
                artifacts: [
                    {
                        name: "example from gui",
                        version: softwareVersion,
                        uri: "uri"
                    }
                ]
            }
        };
        try {
            const responseWfx = await fetch("http://XXX.XXX.XXX.XXX:58081/api/wfx/v1/jobs", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            });
            if (responseWfx.ok) {
                console.log("success.")
                // setIsStagingJob(true)
            } else {
                console.log("failed.")
            }
            setReload(!reload)
        } catch (error) {
            console.error('Request failed', error)
            setIsSendingJobReq(false)
        }
    };

    const handleDeleteSoftware = async () => {
        setIsDeletingSoftware(true)

        try {
            const response = await fetch(`http://XXX.XXX.XXX.XXX:58001/delete/${softwareHashes}.${deviceID}.swu`, {
                method: "DELETE",
            });
            if (!response.ok) {
                throw new Error("Delete requests to file-server failed.")
            }
            const response2 = await fetch(`http://XXX.XXX.XXX.XXX:58002/delete/${deviceID}.swu`, {
                method: "DELETE",
            });
            if (!response2.ok) {
                throw new Error("Delete requests to file-server failed.")
            }
            setExistsSoftware(false)
            setReload(!reload)
        } catch (error) {
            console.error("File upload failed", error)
        } finally {
            setIsDeletingSoftware(false)
        }
    }

    const handleJobDownload = async () => {
        setStatusJobDownload(false)
        const requestBody = {
            clientID: deviceID,
            state: "DOWNLOAD",
            progress: 0
        };
        try {
            const response = await fetch(`http://XXX.XXX.XXX.XXX:58081/api/wfx/v1/jobs/${jobId}/status`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            });
            setReload(!reload)
        } catch (error) {
            setStatusJobDownload(true)
            console.error('Request failed', error)
        }
    }

    const handleJobInstall = async () => {
        setStatusJobInstall(false)
        const requestBody = {
            clientID: deviceID,
            state: "INSTALL",
            progress: 0
        };
        try {
            const response = await fetch(`http://XXX.XXX.XXX.XXX:58081/api/wfx/v1/jobs/${jobId}/status`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            });
            setReload(!reload)
        } catch (error) {
            setStatusJobInstall(true)
            console.error('Request failed', error)
        }
    }

    const handleJobDelete = async () => {
        try {
            const response = await fetch(`http://XXX.XXX.XXX.XXX:58081/api/wfx/v1/jobs/${jobId}`, {
                method: "DELETE",
            });
            setIsStagingJob(false)
            setIsSendingJobReq(false)
            setReload(!reload)
        } catch (error) {
            console.error("File upload failed", error)
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await fetch("http://XXX.XXX.XXX.XXX:58002/metadata/timestamp.json");
                const json1 = await response1.json()
                const versionSnapshot = json1.signed.meta["snapshot.json"].version;

                const urlSnapshot = `http://XXX.XXX.XXX.XXX:58002/metadata/${versionSnapshot}.snapshot.json`;
                const response2 = await fetch(urlSnapshot)
                const json2 = await response2.json()
                const versionTargets = json2.signed.meta['targets.json'].version

                const urlTargets = `http://XXX.XXX.XXX.XXX:58002/metadata/${versionTargets}.targets.json`;
                const response3 = await fetch(urlTargets)
                const json3 = await response3.json()

                if (`${deviceID}.swu` in json3.signed.targets) {
                    const target = json3.signed.targets[`${deviceID}.swu`]
                    setSoftwareVersion(target.version)
                    setSoftwareHashes(target.hashes.sha256)
                    setSoftwareLength(target.length)
                    setExistsSoftware(true)
                }
            } catch (error) {
                console.log("Error fetching data:", error)
            }
        }
        fetchData();
    }, [reload]);

    // handle activity
    useEffect(() => {
        const fetchData = async () => {
            console.log("try fetch~")
            try {
                const res = await fetch(`http://XXX.XXX.XXX.XXX:58080/api/wfx/v1/jobs?clientId=${deviceID}&sort=desc&group=FAILED,CLOSED`);
                if (!res.ok) {
                    throw new Error("(a-1) Failed to fetch data");
                }
                const json = await res.json();
                setJobActivityData(json)
            } catch (err) {
                console.log("(x) Error fetching data:", err)
            }
        }
        fetchData();
        const interval = setInterval(fetchData, 60000);
        return () => clearInterval(interval)
    }, [reload])

    //handle isStagingJob
    useEffect(() => {
        const fetchData = async () => {
            console.log("(isStagingJob) try fetch")
            try {
                const res = await fetch(`http://XXX.XXX.XXX.XXX:58080/api/wfx/v1/jobs?clientId=${deviceID}&group=OPEN`);
                if (!res.ok) {
                    throw new Error("(s-1) Failed to fetch data");
                }
                const json = await res.json();
                const content = json.content
                if (content === undefined) {
                    setIsStagingJob(false)
                    throw new Error("(1) There is no JSON file.");
                }
                if (Object.keys(content).length !== 0) {
                    const data2 = createStagedData(content)
                    if (Object.keys(data2).length !== 0) {
                        setJobId(data2['jobId'])
                        setJobStatusData(data2)
                        setIsStagingJob(true)
                        if (data2.state === "DOWNLOADED"){
                            setStatusJobInstall(true)
                        }
                    } else {
                        setIsStagingJob(false)
                    }

                } else {
                    const res2 = await fetch(`http://XXX.XXX.XXX.XXX:58080/api/wfx/v1/jobs?clientId=${deviceID}&state=CREATED`);
                    if (!res2.ok) {
                        throw new Error("(1) Failed to fetch data");
                    }
                    const json2 = await res2.json();
                    const content2 = json2.content
                    if (content2 === undefined) {
                        throw new Error("(2) There is no JSON file.");
                    }
                    if (Object.keys(content2).length !== 0) {
                        const data = createStagedData(content2)
                        if (Object.keys(data).length !== 0) {
                            setJobId(data['jobId'])
                            setJobStatusData(data)
                            setIsStagingJob(true)
                            if (data.state === "CREATED"){
                                setStatusJobInstall(false)
                                setStatusJobDownload(true)
                            }
                        } else {
                            setIsStagingJob(false)
                        }
                    } else {
                        setIsStagingJob(false)
                    }
                }
            } catch (err) {
                console.log("(s-2) Error fetching data:", err)
            }
        }
        fetchData();
        const interval = setInterval(fetchData, 1000);
        return () => clearInterval(interval)
    }, [])

    const componentSoftware = (
        <div className="flex flex-row text-sm">
            <div className="flex basis-5/6 border border-black text-center rounded">
                <div className="basis-1/4 p-2 text-center  bg-yellow-200">version: {softwareVersion}</div>
                <div className="basis-1/4 p-2 text-center">length: {softwareLength}</div>
                <div className="basis-1/2 p-2 text-center">hash: {softwareHashes.slice(0, 20)}..</div>
            </div>
            <button className='basis-1/6 ml-2 p-2 border border-black rounded' onClick={handleDeleteSoftware} disabled={isDeletingSoftware}>{isDeletingSoftware ? "Deleting..." : "Delete"}</button>
        </div>
    )

    return (
        <>
            <div className="mb-8">
                <div className='text-lg mb-1'>Software</div>
                {existsSoftware ? componentSoftware : componentUpload}
            </div>


            {isStagingJob ? (
                <div className="mb-5">
                    <div className='text-lg mb-1'>Staged job</div>
                    {handleStaged(jobStatusData)}
                    <div className="flex flex-row text-sm">
                        <div className="flex basis-4/6 border border-black text-center rounded">

                            <button disabled={!statusJobDownload} className={`basis-1/2 p-2 text-center  ${statusJobDownload ? 'bg-yellow-200' : ''}`} onClick={handleJobDownload}>Download</button>
                            <button disabled={!statusJobInstall} className={`basis-1/2 p-2 text-center  ${statusJobInstall ? 'bg-yellow-200' : ''}`} onClick={handleJobInstall}>Install</button>
                        </div>
                        <button disabled={statusJobInstall} className="basis-1/6 ml-auto p-2 text-center border border-black rounded" onClick={handleJobDelete}>Delete</button>
                        {/* <button className='basis-1/6 ml-2 p-2 border border-black rounded' onClick={handleDeleteSoftware} disabled={isDeletingSoftware}>{isDeletingSoftware ? "Deleting..." : "Delete"}</button> */}
                    </div>
                </div>

                // </div>
            ) : <></>}

            {!isStagingJob && existsSoftware ? (
                <div className="mb-8">
                    <div className='text-lg mb-1'>Job</div>
                    <div className="flex flex-row text-sm">
                        <div className="basis-5/6 p-2 text-center border border-black rounded">no job</div>
                        <button className='basis-1/6 ml-2 p-2 border border-black rounded' onClick={handleJobSubmit}>{isSendingJobReq ? "Sending..." : "Make job"}</button>
                    </div>
                </div>) : <></>}

            <div className="mb-8">
                <div className='text-lg mb-1'>Activity</div>
                {handleActivity(jobActivityData)}
            </div>
        </>
    )
}
