async function getData() {
    const res = await fetch(`http://localhost:8080/api/wfx/v1/jobs?clientId=qemu`)
  
    if (!res.ok) {
      // This will activate the closest `error.js` Error Boundary
      throw new Error('Failed to fetch data')
    }
  
    return res.json()
  }

export default async function Fetcher() {
  try{
    const data = await getData()
    console.log(data)
  } catch (err) {
    // console.log(err)
  }
  return (
  <div className="">
    {/* {deviceID} */}
  </div>
  )
    }