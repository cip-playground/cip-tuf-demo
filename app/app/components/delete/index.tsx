import { useEffect, useState, useRef } from "react"
import { json } from "stream/consumers"


export const Delete = (deviceID, hashes) => {
    const [isDeleting, setIsDeleting] = useState(false);

    const handleUpload = async () => {
        setIsDeleting(true)

        try {
            const response = await fetch(`http://XXX.XXX.XXX.XXX:58002/delete/${hashes}.${deviceID}.swu`, {
                method: "DELETE",
            });
            if (!response.ok){
                throw new Error("Delete requests to file-server failed.")
            }
            const response2 = await fetch(`http://XXX.XXX.XXX.XXX:58001/delete/${deviceID}.swu`, {
                method: "DELETE",
            });
            if (!response2.ok){
                throw new Error("Delete requests to file-server failed.")
            }
        } catch (error) {
            console.error("File upload failed", error)
        } finally {
            setIsDeleting(false)
        }
    }

    return (
    <div  className='mb-5'>
        <button className='bg-stone-500' onClick={handleUpload} disabled={isDeleting}>{isDeleting ? "Deleting..." : "Delete"}</button>
    </div>)
}
