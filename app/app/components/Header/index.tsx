import Link from 'next/link'

export const Header = () => {
  return (
  <header className="font-mono fixed top-0 left-0 justify-between p-2 h-14 w-full">
    <div className='flex flex-raw flex-wrap justify-center h-10 w-full bg-stone-200 rounded-lg'>
      <div className='flex items-center ml-10'>
        <Link className="text-left justify-center items-center" href="/">🟨 gui</Link>
      </div>
      <div className='flex ml-auto items-center mr-10'>
        <Link className="text-left" href="/">/about</Link>
      </div>
    </div>
  </header>)
}
