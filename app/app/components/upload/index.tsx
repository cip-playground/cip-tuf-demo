import { useEffect, useState, useRef } from "react"


export const Upload = (deviceID, version) => {
    const [file, setFile] = useState("");
    const fileInputRef = useRef("");
    const [isUploading, setIsUploading] = useState(false);

    const handleFileChange = (e) => {
        setFile(e.target.files[0])
    }

    const handleUpload = async () => {
        if (!file) return;

        setIsUploading(true)

        const formData = new FormData();
        formData.append("file", file, deviceID + ".swu")
        try {
            const response = await fetch("http://XXX.XXX.XXX.XXX:58002/upload", {
                method: "POST",
                body: formData,
            });
            const data = await response.json();
            console.log(data);

            if (response.ok) {
                setFile("");
                fileInputRef.current.value = "";
            }
            const response2 = await fetch("http://XXX.XXX.XXX.XXX:58001/upload", {
                method: "POST",
                body: formData,
            });
            const data2 = await response2.json()
            console.log(data2)

        } catch (error) {
            console.error("File upload failed", error)
        } finally {
            setIsUploading(false)
        }
    }

    return (<>
    <div className='text-lg mb-1'>Upload</div>
    <div  className='mb-5'>
        <input type="file" onChange={handleFileChange} ref={fileInputRef}/>
        <button onClick={handleUpload} disabled={isUploading}>{isUploading ? "Uploading..." : "Upload"}</button>
    </div>
    </>)
}
