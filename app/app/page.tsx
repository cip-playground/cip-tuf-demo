import Image from 'next/image'

import Link from 'next/link'

export default function Home() {
  let devices = [
    {id: 1, name: "qemu", icon: "🚀"}, 
    {id: 2, name: "bbb", icon: "🐶"},
    {id: 3, name: "dummy2", icon: "🍎"},
    {id: 3, name: "dummy3", icon: "🍊"}]
  return (
    <main style={{height: 'calc(100vh - 112px)'}}  className="font-mono flex flex-col items-center justify-center">
      <div className="flex items-center justify-center flex-wrap flex-raw">
          {devices.map((device, index) => (
            <Link key={index} href={`/device/${device.name}`}>
              <div key={`LINK-${index}`} className="flex items-center  bg-stone-200 m-3 rounded-lg">
                <div key={`${index}-1`} className='flex items-center justify-center w-16 h-16' ><span className='text-3xl'>{device.icon}</span></div>
                <div key={`${index}-2`} className='flex items-center justify-center w-20 h-16'>{device.name}</div>
              </div>
            </Link>
          ))}
    </div>
    </main>
  )
}
