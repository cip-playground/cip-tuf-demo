package main

/*
#cgo CFLAGS: -I/usr/include/lua5.4
#cgo LDFLAGS: -L/usr/lib -llua5.4
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "stdlib.h"

extern int SetConfig(lua_State *L);
extern int Send(lua_State *L);

static const luaL_Reg heartbeat[] = {
    {"set_config", SetConfig},
    {"send", Send},
    {NULL, NULL} };

static void myLuaL_newlib(lua_State *L) {
    luaL_newlib(L, heartbeat);
}
*/
import "C"
import (
    "fmt"
    "unsafe"

    MQTT "github.com/eclipse/paho.mqtt.golang"

)

type ClientConfig struct {
    Broker   string
    ClientID string
}

type ConfigManager struct {
    config *ClientConfig
}

func (cm *ConfigManager) SetConfig(config *ClientConfig) {
    cm.config = config
}

func (cm *ConfigManager) GetConfig() *ClientConfig {
    return cm.config
}

var configManager = &ConfigManager{}

func getField(L *C.lua_State, fieldName string) (string, error) {
    var len C.size_t
    cFieldName := C.CString(fieldName)
    defer C.free(unsafe.Pointer(cFieldName))
    C.lua_getfield(L, 1, cFieldName)
    if C.lua_type(L, -1) == C.LUA_TNIL {
        return "", fmt.Errorf("%s is missing", fieldName)
    }
    value := C.GoString(C.lua_tolstring(L, -1, &len))
    C.lua_settop(L, -2)
    return value, nil
}

//export SetConfig
func SetConfig(L *C.lua_State) C.int {
    broker, err := getField(L, "broker")
    if err != nil {
        C.lua_pushinteger(L, 0)
        return 1
    }
    clientID, err := getField(L, "clientID")
    if err != nil {
        C.lua_pushinteger(L, 0)
        return 1
    }

    config := &ClientConfig{
        Broker:   broker,
        ClientID: clientID,
    }
    configManager.SetConfig(config)
    C.lua_pushinteger(L, 1)
    return 1
}

//export Send
func Send(L *C.lua_State) C.int {
    value := C.GoString(C.lua_tolstring(L, 1, nil))
    config := configManager.GetConfig()

    opts := MQTT.NewClientOptions().AddBroker(config.Broker)
    opts.SetClientID(config.ClientID)

    client := MQTT.NewClient(opts)
    if token := client.Connect(); token.Wait() && token.Error() != nil {
        C.lua_pushinteger(L, 0)
        return 1
    }

    topic := fmt.Sprintf("device/%s/heartbeat", config.ClientID)
    token := client.Publish(topic, 0, false, value)
    token.Wait()
    if token.Error() != nil {
        C.lua_pushinteger(L, 0)
        return 1
    }

    client.Disconnect(250)
    C.lua_pushinteger(L, 1)
    return 1
}

//export luaopen_heartbeat
func luaopen_heartbeat(L *C.lua_State) C.int {
    C.myLuaL_newlib(L)
    return 1
}

func main() {}
