# Heartbeat

This is a library to confirm the device's survival.
The idea is to send an integer as a status to the Broker as shown below.

```bash
mosquitto_pub -h localhost -p 1883 -t device/exampleDeviceId/heartbeat -m "1"
```

It is used as a library for Lua.

```lua
local heartbeat = require("heartbeat")
local config = {
    broker = broker_url,
    clientID = "exampleDeviceId",
}
heartbeat.set_config(config)
heartbeat.send("0")
```

You can run the sample as shown below.

```bash
lua5.4 example.lua
```

## Build

```bash
go build -o heartbeat.so -buildmode=c-shared main.go
```
