local heartbeat = require("heartbeat")


-- 設定を行う
local config = {
    broker = "tcp://localhost:1883",
    clientID = "lua_mqtt_client",
}
local r = heartbeat.set_config(config)

print(r)

if r == 1 then
    print("Success to set config")
end

-- メッセージを送信する
heartbeat.send("0")
