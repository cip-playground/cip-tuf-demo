inherit dpkg

SRC_URI += "file://heartbeat"

do_prepare_build[cleandirs] += "${S}"
do_prepare_build() {

    cp -r ${WORKDIR}/heartbeat/* ${S}
    deb_add_changelog

}
