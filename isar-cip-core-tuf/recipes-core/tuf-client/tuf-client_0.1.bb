inherit dpkg

SRC_URI += "file://tuf-client"

do_prepare_build[cleandirs] += "${S}"
do_prepare_build() {

    cp -r ${WORKDIR}/tuf-client/* ${S}
    deb_add_changelog

}
