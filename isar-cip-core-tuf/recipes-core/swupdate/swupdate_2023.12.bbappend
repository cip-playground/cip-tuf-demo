FILESEXTRAPATHS:prepend := "${FILE_DIRNAME}/files/:"


# Fix a bug in CURLINFO_SCHEME usage, which causes channel_map_http_code()
# always returns CHANNEL_EBADMSG even for file:// protocol.
SRC_URI:append = " file://channel-curl-Fix-usage-of-CURLINFO-SCHEME.patch"

# disable unnecessary suricatta lua configs
SRC_URI:append = " file://0001-Disable-unnecessary-suricatta-configs.patch"
