# Packages for debugging
IMAGE_PREINSTALL:append = " vim-tiny less"

# Include tuf-client and heartbeat module
IMAGE_INSTALL:append = " tuf-client heartbeat"

# SWU compression
SWU_COMPRESSION_TYPE := "zlib"
