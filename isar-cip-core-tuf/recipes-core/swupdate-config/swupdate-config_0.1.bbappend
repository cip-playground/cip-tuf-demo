# Override the default swupdate.cfg.
# no-downgrading and no-reinstalling are necessary in the current app.
FILESEXTRAPATHS:prepend := "${FILE_DIRNAME}/files/:"

SRC_URI:append:swupdate = " file://suricatta \
			file://swupdate_suricatta.lua \
			file://sw-versions"

SURICATTA_ID:qemu-amd64 = "qemu"
SURICATTA_ID:bbb = "bbb"
do_install:append:swupdate() {

	install -d ${D}/etc/
	install -m 0644 ${WORKDIR}/sw-versions ${D}/etc/

	# custom suricatta lua module for invoking tuf-client
        install -d ${D}/root
        install -m 0644 ${WORKDIR}/swupdate_suricatta.lua ${D}/root

	# Set suricatta id app can handle based on the target machine
	sed -i \
		-e "s@SWU_VERSION@${SWU_VERSION}@g" \
		-e "s@SURICATTA_ID@${SURICATTA_ID}@g" \
		${D}/etc/swupdate.cfg
        sed -i \
		-e "s@SWU_VERSION@${SWU_VERSION}@g" \
		${D}/etc/sw-versions
}

# Helper scripts for demo
SRC_URI:append = " file://0_show-swupdate-log.sh file://1_reset.sh"
do_install:append() {
	install -d ${D}/root
	install -m 0755 ${WORKDIR}/0_show-swupdate-log.sh ${WORKDIR}/1_reset.sh ${D}/root
}
