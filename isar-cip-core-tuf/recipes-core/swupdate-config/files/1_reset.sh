#!/bin/sh

eval $(bg_printenv -c -r)
if ! echo "${KERNEL}" | grep -q "BOOT1"; then
	echo "ERROR; Run this script in B partition"
	exit 1
fi

bg_setenv -c -r 0 -s 0
reboot
