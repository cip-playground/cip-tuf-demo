#!/bin/sh

eval $(bg_printenv -c -r)
if ! echo "${KERNEL}" | grep -q "BOOT0"; then
	echo "ERROR; Run this script in A partition"
	exit 1
fi

journalctl -u swupdate -f
