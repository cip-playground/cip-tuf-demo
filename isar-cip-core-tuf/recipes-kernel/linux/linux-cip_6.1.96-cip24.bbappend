FILESEXTRAPATHS:prepend := "${FILE_DIRNAME}/files/:"

# Enable on-board Ethernet driver (cpsw_new),
# which is now not present in cip_bbb_defconfig
SRC_URI:append = " file://cpsw-switchdev.cfg"
